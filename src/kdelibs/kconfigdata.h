// (C) 1996-1998 by Matthias Kalle Dalheimer

#ifndef KCONFIGDATA_H
#define KCONFIGDATA_H

#include <qdict.h>
#include <qstring.h>

/**
 * Entry-dictionary entry.
 * @internal
 */
struct KEntryDictEntry {
    QString aValue;
    bool bDirty; // must the entry be written back to disk?
};

typedef QDict<KEntryDictEntry> KEntryDict;
typedef QDict<KEntryDict> KGroupDict;
typedef QDictIterator<KEntryDict> KGroupIterator;
typedef QDictIterator<KEntryDictEntry> KEntryIterator;

/**
 * Configuration data manager, used internally by KConfig.
 * @short Configuration data manager, used internally by KConfig.
 * @version $Id: kconfigdata.h,v 1.11 1998/10/07 06:49:24 kalle Exp $
 * @author Matthias Kalle Dalheimer (kalle@kde.org)
 * @internal
 */
class KConfigBaseData
{
    friend class KConfig;
    friend class KConfigBase;

private:
    QString aAppFile;
    QString aGroup;
    bool bDirty; // is there any entry that has to be written back to disk?

    QDict<KEntryDict> aGroupDict;

#ifndef NDEBUG
    QString aFile;
#endif

public:
    KConfigBaseData();
    KConfigBaseData(const char *pAppFile);

    KGroupIterator *groupIterator(void);
};

inline KConfigBaseData::KConfigBaseData()
    : aAppFile(0)
    , aGroup("<default>")
    , bDirty(false)
    , aGroupDict(37, false)
#ifndef NDEBUG
    , aFile(0)
#endif
{
    aGroupDict.setAutoDelete(true);
}

inline KConfigBaseData::KConfigBaseData(const char *pAppFile)
    : aAppFile(pAppFile)
    , aGroup("<default>")
    , bDirty(false)
    , aGroupDict(37, false)
#ifndef NDEBUG
    , aFile(0)
#endif
{
    aGroupDict.setAutoDelete(true);
}

inline KGroupIterator *KConfigBaseData::groupIterator(void)
{
    return new KGroupIterator(aGroupDict);
}

#endif
