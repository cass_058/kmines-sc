/* This file is part of the KDE libraries
    Copyright (C) 1997 Stephan Kulow (coolo@kde.org)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#ifndef KLOCALE_H
#define KLOCALE_H

#include <qstring.h>
#include <qstrlist.h>

/**
 *
 * KLocale provides support for localizing strings and
 * other localization-related stuff.
 *
 * @author Stephan Kulow (coolo@kde.org)
 * @short class for localizing strings
 */
class KLocale
{
public:
    /**
     * Create a KLocale with the given catalogue name.
     * If no catalogue is given, the application name is used.
     * The constructor looks for an entry Locale/Language in the
     * configuration file.
     * If nothing is set there, it looks for the environment variable
     * $LANG. The format for LANG is de:fr:.., if de
     * (german) is your prefered language and fr (french) is your
     * second prefered language. You can add as many languages as
     * you want. If none of them can be find, the default (C) will
     * be used.
     * @param catalogue the name of the language file
     */
    KLocale(const char *catalogue = 0L);

    /**
     * Destructor.
     */
    ~KLocale();

    /**
     * Translate the string into the corresponding string in
     * the locale language, if available. If not, returns
     * the string itself.
     * @param index the lookup text and default text, if not found
     */
    const char *translate(const char *index);

    /**
     * Returns the language used by this object. The domain AND the
     * library translation must be available in this language.
     * 'C' is default, if no other available.
     * @return The language code of the currently used language
     */
    const QString &language() const
    {
        return lang;
    }

    /**
     * Returns the languages selected by user.
     * @return List of language codes
     */
    const QStrList &languages() const
    {
        return langs;
    };

    /**
     * Returns the charset name used by selected locale.
     * Special file with charset name must be available
     * "us-ascii" is default
     */
    const QString &charset() const
    {
        return chset;
    }

    /**
     * Adds another catalogue to search for translation lookup.
     * This function is useful for extern libraries and/or code,
     * that provides it's own messages.
     *
     * If the catalogue does not exist for the chosen language,
     * it will be ignored and C will be used.
     **/
    void insertCatalogue(const char *catalogue);

    /**
     * Returns the parts of the first parameter understood as
     * language settings. The format is language_country.charset
     */
    static void splitLocale(const QString &str, QString &language, QString &country, QString &charset);

    /**
     * It works like splitLocale() but in reverse.
     */
    static const QString mergeLocale(const QString &lang, const QString &country, const QString &charset);

private:
    QStrList *catalogues;
    QString lang;
    QString chset;
    QStrList langs;

    // Disallow assignment and copy-construction
    KLocale(const KLocale &);
    KLocale &operator=(const KLocale &);
};

#endif
