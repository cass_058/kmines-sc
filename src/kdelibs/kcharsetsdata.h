/* This file is part of the KDE libraries
    Copyright (C) 1997 Jacek Konieczny (jajcus@zeus.polsl.gliwice.pl)
    $Id: kcharsetsdata.h,v 1.18 1998/11/30 08:22:01 kalle Exp $

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
    */

#ifndef KCHARSETSDATA_H
#define KCHARSETSDATA_H

//#define KCH_DEBUG

#include <kcharsets.h>
#include <qfont.h>

#define CHAR_TAGS_COUNT (nrOfTags)

/**
 * Charset internal structure.
 * @internal
 */
struct KCharsetEntry {
    const char *name;
    QFont::CharSet qtCharset;
    bool registered;
    QString *good_family; /* it must be pointer so charset entries can be compiled-in */
};

/**
 * Charset support structure.
 * @internal
 */
struct KCharTags {
    const char *tag;
    unsigned code;
};

/**
 * Charset support structure.
 * @internal
 */
struct KDispCharEntry {
    KCharsetEntry *charset;
    unsigned code;
};

class KCharsetsData
{
    static KCharsetEntry charsets[];
    const KCharsetEntry *defaultCh;

public:
    KCharsetsData();
    ~KCharsetsData();
    const char *faceForCharset(const KCharsetEntry *charset);
    QString charsetFace(const KCharsetEntry *charset, const QString &face);
    bool charsetOfFace(const KCharsetEntry *charset, const QString &face);
    const KCharsetEntry *charsetOfFace(const QString &face);
    const KCharsetEntry *charsetEntry(const char *name);
    const KCharsetEntry *charsetEntry(int index);
    const KCharsetEntry *charsetEntry(QFont::CharSet);
    const KCharsetEntry *defaultCharset() const
    {
        return defaultCh;
    }
    bool setDefaultCharset(const KCharsetEntry *charset);

private:
    // Disallow assignment and copy-construction
    KCharsetsData(const KCharsetsData &) {};
    KCharsetsData &operator=(const KCharsetsData &)
    {
        return *this;
    }
};

#ifdef KCH_DEBUG
void kchdebug(const char *msg, ...);
#else /* KCH_DEBUG */
inline void kchdebug(const char *, ...)
{
}
#endif /* KCH_DEBUG */

#endif
