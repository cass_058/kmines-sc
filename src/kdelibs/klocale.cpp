/* This file is part of the KDE libraries
    Copyright (C) 1997 Stephan Kulow (coolo@kde.org)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include "klocale.h"
#include "kapp.h"
#include "kcharsets.h"
#include "kconfig.h"

#include "config.h"

#include <qdir.h>

#include <libintl.h>
#include <locale.h>
#include <stdlib.h>

#define SYSTEM_MESSAGES "kde"

static QStrList makeLanguagesList(const QString &languages)
{
    // a string list to be returned
    QStrList list;
    // temporary copy of languages string
    QString str = languages;
    str.detach();

    while (!str.isEmpty()) {
        int f = str.find(':');
        if (f >= 0) {
            list.append(str.left(f));
            str = str.right(str.length() - f - 1);
        } else {
            list.append(str);
            str = "";
        }
    }
    return list;
}

const QString KLocale::mergeLocale(const QString &lang, const QString &country, const QString &charset)
{
    if (lang.isEmpty())
        return "C";
    QString ret = lang;
    if (!country.isEmpty())
        ret += "_" + country;
    if (!charset.isEmpty())
        ret += "." + charset;
    return ret;
}

void KLocale::splitLocale(const QString &aStr, QString &lang, QString &country, QString &chset)
{
    QString str = aStr.copy();

    // just in case, there is another language appended
    int f = str.find(':');
    if (f >= 0) {
        str = str.left(f);
    }

    country = "";
    chset = "";
    lang = "";

    f = str.find('.');
    if (f >= 0) {
        chset = str.right(str.length() - f - 1);
        str = str.left(f);
    }

    f = str.find('_');
    if (f >= 0) {
        country = str.right(str.length() - f - 1);
        str = str.left(f);
    }

    lang = str;
}

#ifdef ENABLE_NLS

KLocale::KLocale(const char *catalogue)
{
    /* Set locale via LC_ALL according to environment variables  */
    setlocale(LC_ALL, "");
    chset = "us-ascii";
    if (!catalogue)
        catalogue = kapp->appName().data();

    catalogues = new QStrList(true);
    catalogues->append(catalogue);

    QString languages;
    const char *g_lang = getenv("KDE_LANG");
    languages = g_lang;

    bool set_locale_vars = false;

    if (kapp) {
        QString setting;
        KConfig *config = kapp->getConfig();
        config->setGroup("Locale");
        if (!g_lang)
            languages = config->readEntry("Language", "default");
        setting = config->readEntry("Collate", "default");
        if (setting != "default")
            setlocale(LC_COLLATE, setting);
        setting = config->readEntry("Time", "default");
        if (setting != "default")
            setlocale(LC_TIME, setting);
        setting = config->readEntry("Monetary", "default");
        if (setting != "default")
            setlocale(LC_MONETARY, setting);
        setting = config->readEntry("CType", "default");
        if (setting != "default")
            setlocale(LC_CTYPE, setting);
        setting = config->readEntry("Numeric", "default");
        if (setting != "default")
            setlocale(LC_NUMERIC, setting);
        set_locale_vars = config->readBoolEntry("SetLocaleVariables", false);
    } else if (!g_lang)
        languages = "default";

    // setlocale reads variables LC_* and LANG, and it may use aliases,
    // so we don't have to do it
    g_lang = setlocale(LC_MESSAGES, 0);

    if (languages.isEmpty() || (languages == "default")) {
        if (g_lang && g_lang[0] != 0) // LANG value is set and is not ""
            languages = g_lang;
        else
            languages = "C";
    } else
        languages = languages + ":C";

    QString directory = KApplication::kde_localedir();
    QString ln, ct, chrset;

    // save languages list requested by user
    langs = makeLanguagesList(languages);
    while (true) {
        int f = languages.find(':');
        if (f > 0) {
            lang = languages.left(f);
            languages = languages.right(languages.length() - lang.length() - 1);
        } else {
            lang = languages;
            languages = "";
        }

        if (lang.isEmpty() || lang == "C")
            break;

        splitLocale(lang, ln, ct, chrset);

        QString lng[3];
        lng[0] = ln + "_" + ct + "." + chrset;
        lng[1] = ln + "_" + ct;
        lng[2] = ln;
        int i;
        for (i = 0; i < 3; i++) {
            QDir d(directory + "/" + lng[i] + "/LC_MESSAGES");
            if (d.exists(QString(catalogue) + ".mo") && d.exists(QString(SYSTEM_MESSAGES) + ".mo")) {
                lang = lng[i];
                break;
            }
        }

        if (i != 3)
            break;
    }

    chset = chrset;

    setlocale(LC_MESSAGES, lang);
    if (set_locale_vars) {
        // set environment variables for all categories
        // maybe we should treat LC_NUMERIC differently (netscape problem)
        QString stmp;
        stmp = QString("LC_MESSAGES") + "=" + setlocale(LC_MESSAGES, 0);
        putenv(stmp.data());
        stmp = QString("LC_CTYPE") + "=" + setlocale(LC_CTYPE, 0);
        putenv(stmp.data());
        stmp = QString("LC_COLLATE") + "=" + setlocale(LC_COLLATE, 0);
        putenv(stmp.data());
        stmp = QString("LC_TIME") + "=" + setlocale(LC_TIME, 0);
        putenv(stmp.data());
        stmp = QString("LC_NUMERIC") + "=" + setlocale(LC_NUMERIC, 0);
        putenv(stmp.data());
        stmp = QString("LC_MONETARY") + "=" + setlocale(LC_MONETARY, 0);
        putenv(stmp.data());
    }
    // we should use LC_CTYPE, not LC_MESSAGES for charset
    // however in most cases it should be the same for messages
    // to be readable (there is no i18n messages charset conversion)
    // So when LC_CTYPE is not set (is set to "C") better stay
    // with LC_MESSAGES
    QString lc_ctype = setlocale(LC_CTYPE, 0);
    if (!lc_ctype.isEmpty() && lc_ctype != "C") {
        splitLocale(setlocale(LC_CTYPE, 0), ln, ct, chrset);
        if (!chrset.isEmpty())
            chset = chrset;
    }

    insertCatalogue(catalogue);
    insertCatalogue(SYSTEM_MESSAGES);
    if (chset.isEmpty() || !KCharset(chset).ok())
        chset = "us-ascii";
}

KLocale::~KLocale()
{
    delete catalogues;
}

void KLocale::insertCatalogue(const char *catalogue)
{
    bindtextdomain(catalogue, KApplication::kde_localedir());
    catalogues->append(catalogue);
}

const char *KLocale::translate(const char *msgid)
{
    const char *text = msgid;
    for (const char *catalogue = catalogues->first(); catalogue; catalogue = catalogues->next()) {
        text = dgettext(catalogue, msgid);
        if (text != msgid) // we found it
            break;
    }
    return text;
}

#else /* ENABLE_NLS */

KLocale::KLocale( const char *)
{
    lang = "C";
    chset = "us-ascii";
    langs = makeLanguagesList(lang);
}

KLocale::~KLocale()
{
}

void KLocale::insertCatalogue(const char *)
{
}

const char *KLocale::translate(const char *msgid)
{
    return msgid;
}

#endif /* ENABLE_NLS */
