/* This file is part of the KDE libraries
    Copyright (C) 1997 Nicolas Hadacek <hadacek@via.ecp.fr>
    Copyright (C) 1997 Mario Weilguni <mweilguni@sime.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#ifndef KKEYDIALOG_H
#define KKEYDIALOG_H

#include <kaccel.h>
#include <kapp.h>

#include <qbuttongroup.h>
#include <qcheckbox.h>
#include <qdialog.h>
#include <qdict.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlist.h>
#include <qlistbox.h>
#include <qobject.h>
#include <qpushbutton.h>
#include <qstring.h>

/**
 * This class is used internally.
 */
class KButtonBoxItem
{
public:
    QPushButton *button;
    bool noexpand;
    int stretch;
    int actual_size;
};

/**
 * A container widget for buttons. Uses Qt layout control to place the
 * buttons, can handle both vertical and horizontal button placement.
 * @short A container widget for buttons
 */
class KButtonBox : public QWidget
{
    Q_OBJECT

public:
    enum { VERTICAL = 1, HORIZONTAL = 2 };

    /**
     * Creates an empty container for buttons. If _orientation is
     * KButtonBox::VERTICAL, the buttons inserted with @see addButton
     * are layouted from top to bottom, otherwise they are layouted
     * from left to right.
     */
    KButtonBox(QWidget *parent, int _orientation = HORIZONTAL, int border = 0, int _autoborder = 6);

    /**
     * The destructor is needed, otherwise gcc 2.7.2.1 may report an
     * internal compiler error. It does nothing.
     */
    ~KButtonBox();

    /**
     * @return the minimum size needed to fit all buttons. This size is
     * calculated by the with/height of all buttons plus border/autoborder
     */
    virtual QSize sizeHint() const;

    virtual void resizeEvent(QResizeEvent *);

    /**
     * adds a new @see QPushButton and @return a pointer to the newly
     * created button. If noexpand is FALSE, the width of the button is
     * adjusted to fit the other buttons (the maximum of all buttons is
     * taken). If noexpand is TRUE, the width of this button will be
     * set to the minimum width needed for the given text).
     */
    QPushButton *addButton(const char *text, bool noexpand = FALSE);

    /**
     * This adds a stretch to the buttonbox. @see QBoxLayout for details.
     * Can be used to separate buttons (i.e. if you add the buttons "OK",
     * "Cancel", add a stretch and then add the button "Help", "OK" and
     * "Cancel" will be left-aligned (or top-aligned for vertical) while
     * "Help" will be right-aligned (or bottom-aligned for vertical).
     */
    void addStretch(int scale = 1);

    /**
     * This function must be called ONCE after all buttons have been
     * inserted. It will start layout control.
     */
    void layout();

protected:
    /**
     * @return the best size for a button. Checks all buttons and takes
     * the maximum width/height.
     */
    QSize bestButtonSize() const;
    void placeButtons();
    QSize buttonSizeHint(QPushButton *) const;

protected:
    int _border, _autoborder;
    int orientation;
    bool activated;
    QList<KButtonBoxItem> buttons;
};

/**
 * A list box item for KSplitList. It uses two columns to display
 * action/key combination pairs.
 * @short A list box item for KSplitList
 */
class KSplitListItem : public QObject, public QListBoxItem
{
    Q_OBJECT

public:
    KSplitListItem(const char *s, int _id = 0);
    ~KSplitListItem() {};
    int getId()
    {
        return id;
    }

protected:
    virtual void paint(QPainter *);
    virtual int height(const QListBox *) const;
    virtual int width(const QListBox *) const;

public slots:
    void setWidth(int newWidth);

protected:
    int halfWidth;
    QString keyName;
    QString actionName;
    int id;
};

/**
 * A list box that can report its width to the items it
 * contains. Thus it can be used for multi column lists etc.
 * @short A list box capable of multi-columns
 */
class KSplitList : public QListBox
{
    Q_OBJECT

public:
    KSplitList(QWidget *parent = 0, const char *name = 0);
    ~KSplitList()
    {
    }
    int getId(int index)
    {
        return ((KSplitListItem *)(item(index)))->getId();
    }

signals:
    void newWidth(int newWidth);

protected:
    void resizeEvent(QResizeEvent *);
    void paletteChange(const QPalette &oldPalette);
    void styleChange(GUIStyle);

protected:
    QColor selectColor;
    QColor selectTextColor;
};

/**
 * A push button that looks like a keyboard key.
 * @short A push button that looks like a keyboard key
 */
class KKeyButton : public QPushButton
{
    Q_OBJECT

public:
    KKeyButton(const char *name = 0, QWidget *parent = 0);
    ~KKeyButton();
    void setText(QString text);
    void setEdit(bool edit);
    bool editing;

protected:
    void paint(QPainter *_painter);
    void drawButton(QPainter *p)
    {
        paint(p);
    }
};

/**
 * The KKeyDialog class is used for configuring dictionaries of key/action
 * associations for KAccel. It uses the KKeyChooser widget and offers buttons
 * to set all keys to defaults and invoke on-line help.
 *
 * Two static methods are supplied which provide the most convienient interface
 * to the dialog. For example you could use KAccel and KKeyDialog like this
 *
 * KAccel keys;
 *
 * keys.insertItem( i18n("Zoom in" ), "Zoom in", "+" );
 * keys.connectItem( "Zoom in", myWindow, SLOT( zoomIn() ) );
 *
 * keys.connectItem( KAccel::Print, myWindow, SLOT( print() ) );
 *
 * keys.readSettings();
 *
 * if( KKeyDialog::configureKeys( this, &keys ) ) {
 *		...
 *	}
 *
 * This will also implicitely save the settings. If you don't want this, you can call
 *
 * if( KKeyDialog::configureKeys( this, &keys, false ) ) { // do not save settings
 *		...
 *	}
 *
 */
class KKeyDialog : public QDialog
{
    Q_OBJECT

public:
    KKeyDialog(QDict<KKeyEntry> *aKeyDict, QWidget *parent = 0);
    ~KKeyDialog() {};

    static int configureKeys(QWidget *parent, KAccel *keys, bool save_settings = true);

private:
    QPushButton *bDefaults, *bOk, *bCancel, *bHelp;
};

/**
 * The KKeyChooser widget is used for configuring dictionaries of key/action
 * associations for KAccel.
 *
 * The class takes care of all aspects of configuration, including handling key
 * conflicts internally. Connect to the allDefault slot if you want to set all
 * configurable keybindings to their default values.
 */
class KKeyChooser : public QWidget
{
    Q_OBJECT

public:
    enum { NoKey = 1, DefaultKey, CustomKey };

    KKeyChooser(QDict<KKeyEntry> *aKeyDict, QWidget *parent = 0);
    ~KKeyChooser();

    QDictIterator<KKeyEntry> *aIt;
    QDictIterator<KKeyEntry> *aKeyIt;

signals:
    void keyChange();

public slots:
    void allDefault();
    void listSync();

protected slots:
    void toChange(int index);
    void changeKey();
    void updateAction(int index);
    void defaultKey();
    void noKey();
    void keyMode(int m);
    void shiftClicked();
    void ctrlClicked();
    void altClicked();
    void editKey();
    void editEnd();

protected:
    void keyPressEvent(QKeyEvent *e);
    void fontChange(const QFont &);

protected:
    KKeyEntry *pEntry;
    QString sEntryKey;
    KSplitList *wList;
    QLabel *lInfo, *lNotConfig;
    QLabel *actLabel, *keyLabel;
    KKeyButton *bChange;
    QCheckBox *cShift, *cCtrl, *cAlt;
    QGroupBox *fCArea;
    QButtonGroup *kbGroup;

    bool bKeyIntercept;

    int kbMode;

    const QString item(uint keyCode, const QString &entryKey);
    bool isKeyPresent();
    void setKey(uint kCode);
};

#endif
