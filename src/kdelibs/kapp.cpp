/* This file is part of the KDE libraries
    Copyright (C) 1997 Matthias Kalle Dalheimer (kalle@kde.org)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
    */

#include "kapp.h"
#include "kcharsets.h"
#include "klocale.h"

#include "config.h"

#include <qdir.h>
#include <qfile.h>
#include <qkeycode.h>
#include <qmessagebox.h>
#include <qobjcoll.h>
#include <qregexp.h>
#include <qstrlist.h>
#include <qtextstream.h>
#include <qwidcoll.h>

#include <X11/Xatom.h>
#include <X11/Xlib.h>

#include <fcntl.h>
#include <paths.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

KApplication *KApplication::KApp = 0L;

KApplication::KApplication(int &argc, char **argv)
    : QApplication(argc, argv)
{
    QString aArgv0 = argv[0];
    int nSlashPos = aArgv0.findRev('/');
    if (nSlashPos != -1)
        aAppName = aArgv0.remove(0, nSlashPos + 1);
    else
        aAppName = aArgv0;

    init();

    parseCommandLine(argc, argv);
}

KApplication::KApplication(int &argc, char **argv, const QString &rAppName)
    : QApplication(argc, argv)
{
    aAppName = rAppName;

    init();

    parseCommandLine(argc, argv);
}

void KApplication::init()
{
    // this is important since we fork() to launch the help (Matthias)
    fcntl(ConnectionNumber(qt_xdisplay()), F_SETFD, 1);

    KApp = this;
    bLocaleConstructed = false; // no work around mutual dependencies

    // create the config directory ~/.kde/share/config
    QString configPath = KApplication::localkdedir();
    // Don't access if ~/ is unknown.
    if (QDir::home() != QDir::root()) {
        if (mkdir(configPath.data(), 0755) == 0) { // make it public(?)
            configPath += "/share";
            if (mkdir(configPath.data(), 0755) == 0) { // make it public
                configPath += "/config";
                mkdir(configPath.data(), 0700); // make it private
            }
        }
    }

    // Read the application config file
    QString aConfigName = KApplication::localconfigdir() + "/" + aAppName + "rc";
    QFile aConfigFile(aConfigName);
    bool bSuccess;

    // Open the application config file. It will be created if it does not exist yet.
    bSuccess = aConfigFile.open(IO_ReadWrite);
    if (!bSuccess) {
        // we didn't succeed to open the app-config file
        pConfig = new KConfig(0);
        eConfigState = APPCONFIG_NONE;
    } else {
        // we succeeded to open an app-config file
        pConfig = new KConfig(aConfigName);
        eConfigState = APPCONFIG_OK;
    }

    pCharsets = new KCharsets();

    pLocale = new KLocale(aAppName);
    bLocaleConstructed = true;

    readSettings();
    setPalette();
    setStyleAndFont();

    pTopWidget = 0L;
}

QPopupMenu *KApplication::getHelpMenu(bool /*bAboutQtMenu*/, const char *aboutAppText)
{
    QPopupMenu *pMenu = new QPopupMenu();

    int id = pMenu->insertItem(i18n("&Contents"));
    pMenu->connectItem(id, this, SLOT(appHelpActivated()));
    pMenu->setAccel(Key_F1, id);

    pMenu->insertSeparator();

    id = pMenu->insertItem(QString(i18n("&About")) + " " + aAppName + "...");
    if (aboutAppText) {
        pMenu->connectItem(id, this, SLOT(aboutApp()));
        aAppAboutString = aboutAppText;
    }

    id = pMenu->insertItem(i18n("About &KDE..."));
    pMenu->connectItem(id, this, SLOT(aboutKDE()));
    /*
      if( bAboutQtMenu )
      {
      id = pMenu->insertItem( i18n( "About Qt" ) );
      pMenu->connectItem( id, this, SLOT( aboutQt() ) );
      }
    */
    return pMenu;
}

void KApplication::appHelpActivated()
{
    invokeHTMLHelp(aAppName + "/" + "index.html", "");
}

void KApplication::aboutKDE()
{
    QMessageBox::about(this->mainWidget(),
                       i18n("About KDE"),
                       i18n("\nThe KDE Desktop Environment was written by the KDE Team,\n"
                            "a world-wide network of software engineers committed to\n"
                            "free software development.\n\n"
                            "Visit http://www.kde.org for more information on the KDE\n"
                            "Project. Please consider joining and supporting KDE.\n\n"
                            "Please report bugs at http://bugs.kde.org.\n"));
}

void KApplication::aboutApp()
{
    QMessageBox::about(this->mainWidget(), getCaption(), aAppAboutString);
}

void KApplication::aboutQt()
{
    //  QMessageBox::aboutQt( NULL, getCaption() );
}

KLocale *KApplication::getLocale()
{
    if (!pLocale)
        pLocale = new KLocale();

    return pLocale;
}

void KApplication::parseCommandLine(int &argc, char **argv)
{
    enum parameter_code { unknown = 0, caption, icon };
    const char *parameter_strings[] = { "--caption", "--icon", 0 };

    int i = 1;
    parameter_code parameter;
    while (i < argc) {
        parameter = unknown;

        for (int p = 0; parameter_strings[p]; p++)
            if (!strcmp(argv[i], parameter_strings[p])) {
                parameter = static_cast<parameter_code>(p + 1);
                break;
            }

        if (parameter != unknown && argc < i + 2) { // last argument without parameters
            argc -= 1;
            break; // jump out of the while loop
        }

        switch (parameter) {
        case caption:
            aCaption = argv[i + 1];
            break;
        case icon:
            if (argv[i + 1][0] == '/')
                aIconPixmap = QPixmap(argv[i + 1]);
            else
                aIconPixmap = loadIcon(argv[i + 1]);
            break;
        case unknown:
            i++;
        }

        if (parameter != unknown) { // remove arguments
            for (int j = i; j < argc - 2; j++)
                argv[j] = argv[j + 2];
            argc -= 2;
        }
    }

    if (aIconPixmap.isNull()) {
        aIconPixmap = loadIcon(aAppName + ".xpm");
    }
}

QPixmap KApplication::loadIcon(const QString &name)
{
    QString full_path;
    QPixmap pixmap;

    if (name.left(1) == "/")
        full_path = name;
    else {
        QFileInfo finfo;
        full_path = localicondir() + '/' + name;
        finfo.setFile(full_path);
        if (!finfo.exists())
            full_path = kde_icondir() + '/' + name;
    }

    pixmap.load(full_path);

    return pixmap;
}

KApplication::~KApplication()
{
    if (pLocale)
        delete pLocale;

    delete pCharsets;

    delete pConfig;

    KApp = 0;
}

void KApplication::applyGUIStyle(GUIStyle newstyle)
{
    QApplication::setStyle(applicationStyle);

    // get list of toplevels
    QWidgetList *wl = QApplication::topLevelWidgets();
    QWidgetListIt wl_it(*wl);

    // foreach toplevel ...
    while (wl_it.current()) {
        QWidget *w = wl_it.current();

        // set new style
        w->setStyle(newstyle);
        QObjectList *ol = w->queryList("QWidget", 0, 0, TRUE);
        QObjectListIt ol_it(*ol);

        // set style to child widgets
        while (ol_it.current()) {
            QWidget *child = (QWidget *)(ol_it.current());
            child->setStyle(newstyle);
            ++ol_it;
        }
        delete ol;
        ++wl_it;
    }

    delete wl;
}

const char *KApplication::getCaption() const
{
    if (!aCaption.isNull())
        return aCaption;
    else
        return aAppName;
}

void KApplication::readSettings()
{
    KConfig *config = getConfig();
    config->reparseConfiguration();

    QString str;

    // Read the color scheme group from config file
    // If unavailable set color scheme to KDE default

    config->setGroup("Appearance");
    // this default is Qt black
    textColor = config->readColorEntry("foreground", &black);

    // this default is the Qt lightGray
    backgroundColor = config->readColorEntry("background", &lightGray);

    // this default is Qt darkBlue
    selectColor = config->readColorEntry("selectBackground", &darkBlue);

    // this default is Qt white
    selectTextColor = config->readColorEntry("selectForeground", &white);

    // this default is Qt white
    windowColor = config->readColorEntry("windowBackground", &white);

    // this default is Qt black
    windowTextColor = config->readColorEntry("windowForeground", &black);

    // this default is Qt lightGray
    inactiveTitleColor = config->readColorEntry("inactiveBackground", &lightGray);

    // this default is Qt darkGrey
    inactiveTextColor = config->readColorEntry("inactiveForeground", &darkGray);

    // this default is Qt darkBlue
    activeTitleColor = config->readColorEntry("activeBackground", &darkBlue);

    // this default is Qt white
    activeTextColor = config->readColorEntry("activeForeground", &white);

    contrast = config->readNumEntry("contrast", 7);

    //  Read the font specification from config.
    //  Initialize fonts to default first or it won't work

    pCharsets->setDefault(klocale->charset());
    generalFont = QFont("helvetica", 12, QFont::Normal);
    pCharsets->setQFont(generalFont);

    generalFont = config->readFontEntry("font", &generalFont);

    // Finally, read GUI style from config.

    if (config->readEntry("widgetStyle", "Windows 95") == "Windows 95")
        applicationStyle = WindowsStyle;
    else
        applicationStyle = MotifStyle;
}

void KApplication::setPalette()
{
    // WARNING : QApplication::setPalette() produces inconsistent results.
    // There are 3 problems :-
    // 1) You can't change select colors
    // 2) You need different palettes to apply the same color scheme to
    //		different widgets !!
    // 3) Motif style needs a different palette to Windows style.

    int highlightVal, lowlightVal;

    highlightVal = 100 + (2 * contrast + 4) * 16 / 10;
    lowlightVal = 100 + (2 * contrast + 4) * 10;

    // printf("contrast = %d\n", contrast);

    if (applicationStyle == MotifStyle) {
        QColorGroup disabledgrp(textColor,
                                backgroundColor,
                                backgroundColor.light(highlightVal),
                                backgroundColor.dark(lowlightVal),
                                backgroundColor.dark(120),
                                backgroundColor.dark(120),
                                windowColor);

        QColorGroup colgrp(textColor,
                           backgroundColor,
                           backgroundColor.light(highlightVal),
                           backgroundColor.dark(lowlightVal),
                           backgroundColor.dark(120),
                           textColor,
                           windowColor);

        QApplication::setPalette(QPalette(colgrp, disabledgrp, colgrp), TRUE);

    } else {
        QColorGroup disabledgrp(textColor,
                                backgroundColor,
                                backgroundColor.light(150),
                                backgroundColor.dark(),
                                backgroundColor.dark(120),
                                backgroundColor.dark(120),
                                windowColor);

        QColorGroup colgrp(textColor, backgroundColor, backgroundColor.light(150), backgroundColor.dark(), backgroundColor.dark(120), textColor, windowColor);

        QApplication::setWinStyleHighlightColor(selectColor);
        QApplication::setPalette(QPalette(colgrp, disabledgrp, colgrp), TRUE);
    }
}

void KApplication::setStyleAndFont()
{
    QApplication::setFont(generalFont, TRUE);
    // setStyle() works but may not change the style of combo boxes.
    applyGUIStyle(applicationStyle);

    resizeAll();
}

void KApplication::resizeAll()
{
    // send a resize event to all windows so that they can resize children
    QWidgetList *widgetList = QApplication::topLevelWidgets();
    QWidgetListIt it(*widgetList);

    while (it.current()) {
        it.current()->resize(it.current()->size());
        ++it;
    }
    delete widgetList;
}

void KApplication::invokeHTMLHelp(QString filename, QString topic) const
{
    if (fork() == 0) {
        if (filename.isEmpty())
            filename = aAppName + "/index.html";

        QString path = KApplication::kde_htmldir().copy() + "/";

        // first try the locale setting
        QString file = path + klocale->language() + '/' + filename;
        if (!QFileInfo(file).exists())
            // not found: use the default
            file = path + "default/" + filename;

        if (!topic.isEmpty()) {
            file.append("#");
            file.append(topic);
        }

        const char *shell = "/bin/sh";
        if (getenv("SHELL"))
            shell = getenv("SHELL");
        file.prepend("xdg-open ");
        execl(shell, shell, "-c", file.data(), 0L);
        exit(1);
    }
}

QString KApplication::kdedir()
{
    static QString kdedir;

    if (kdedir.isEmpty()) {
        kdedir = getenv("KDEDIR");
        if (kdedir.isEmpty())
            kdedir = KDEDIR;
    }

    return kdedir;
}

const QString &KApplication::kde_htmldir()
{
    static QString dir;
    if (dir.isNull()) {
        dir = KDE_HTMLDIR;
        if (!strncmp(dir.data(), "KDEDIR", 6))
            dir = kdedir() + dir.right(dir.length() - 6);
    }
    return dir;
}

const QString &KApplication::kde_icondir()
{
    static QString dir;
    if (dir.isNull()) {
        dir = KDE_ICONDIR;
        if (!strncmp(dir.data(), "KDEDIR", 6))
            dir = kdedir() + dir.right(dir.length() - 6);
    }
    return dir;
}

const QString &KApplication::kde_localedir()
{
    static QString dir;
    if (dir.isNull()) {
        dir = KDE_LOCALEDIR;
        if (!strncmp(dir.data(), "KDEDIR", 6))
            dir = kdedir() + dir.right(dir.length() - 6);
    }
    return dir;
}

QString KApplication::localkdedir()
{
    return (QDir::homeDirPath() + "/.kde");
}

QString KApplication::localicondir()
{
    return (localkdedir() + "/share/icons");
}

QString KApplication::localconfigdir()
{
    return (localkdedir() + "/share/config");
}

void KApplication::setMainWidget(QWidget *widget)
{
    // set the specified icon
    widget->setIcon(getIcon());
    // set a short icon text
    widget->setIconText(getCaption());
    // set the application main widget
    QApplication::setMainWidget(widget);
}
