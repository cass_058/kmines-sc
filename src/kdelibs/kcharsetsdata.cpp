/* This file is part of the KDE libraries
    Copyright (C) 1997 Jacek Konieczny (jajcus@zeus.polsl.gliwice.pl)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
    */

#include "kcharsetsdata.h"
#include "kapp.h"

#include <qdir.h>
#include <qfile.h>
#include <qfontinfo.h>
#include <qregexp.h>
#include <qstrlist.h>

#include <X11/Xlib.h>

#include <stdio.h>
#ifdef KCH_DEBUG
#include <stdarg.h>
inline void kchdebug(const char *msg, ...)
{
    va_list ap;
    va_start(ap, msg); // use variable arg list
    vfprintf(stderr, msg, ap);
    va_end(ap);
}
#endif

KCharsetEntry KCharsetsData::charsets[] = { { "us-ascii", QFont::AnyCharSet, TRUE, 0 },
                                            { "iso-8859-1", QFont::ISO_8859_1, TRUE, 0 },
                                            { "iso-8859-2", QFont::ISO_8859_2, TRUE, 0 },
                                            { "iso-8859-3", QFont::ISO_8859_3, TRUE, 0 },
                                            { "iso-8859-4", QFont::ISO_8859_4, TRUE, 0 },
                                            { "iso-8859-5", QFont::ISO_8859_5, TRUE, 0 },
                                            { "iso-8859-6", QFont::ISO_8859_6, TRUE, 0 },
                                            { "iso-8859-7", QFont::ISO_8859_7, TRUE, 0 },
                                            { "iso-8859-8", QFont::ISO_8859_8, TRUE, 0 },
                                            { "iso-8859-9", QFont::ISO_8859_9, TRUE, 0 },
                                            { "adobe-symbol", QFont::AnyCharSet, FALSE, 0 },
                                            { "koi8-r", QFont::KOI8R, TRUE, 0 },
                                            { 0, QFont::AnyCharSet, FALSE, 0 } };

static struct {
    const char *charset;
    const char *face;
} faces[] = { { "adobe-symbol", "symbol" }, { "koi8-r", "*_koi8" }, { 0, 0 } };

/////////////////////////////////////////////////

KCharsetsData::KCharsetsData()
{
}

KCharsetsData::~KCharsetsData()
{
}

const KCharsetEntry *KCharsetsData::charsetEntry(const char *name)
{
    for (int i = 0; charsets[i].name; i++)
        if (stricmp(name, charsets[i].name) == 0) {
            kchdebug("Found!\n");
            return charsets + i;
        }

    return 0;
}

const KCharsetEntry *KCharsetsData::charsetEntry(int index)
{
    for (int i = 0; charsets[i].name; i++)
        if (i == index)
            return charsets + i;

    return 0;
}

const KCharsetEntry *KCharsetsData::charsetEntry(QFont::CharSet qtCharset)
{
    for (int i = 0; charsets[i].name; i++)
        if (charsets[i].qtCharset == qtCharset)
            return charsets + i;

    return 0;
}

bool KCharsetsData::setDefaultCharset(const KCharsetEntry *charset)
{
    if (charset) {
        defaultCh = charset;
        return TRUE;
    }
    return FALSE;
}

QString KCharsetsData::charsetFace(const KCharsetEntry *charset, const QString &face)
{
    const char *faceStr = 0;
    for (int i = 0; faces[i].charset; i++)
        if (stricmp(faces[i].charset, charset->name) == 0) {
            faceStr = faces[i].face;
            break;
        }
    if (!faceStr)
        return face;
    QString newFace(faceStr);
    newFace.replace(QRegExp("\\*"), face);
    return newFace;
}

bool KCharsetsData::charsetOfFace(const KCharsetEntry *charset, const QString &face)
{
    kchdebug("Testing if face %s is of charset %s...", (const char *)face, charset->name);
    const char *faceStr = 0;
    for (int i = 0; faces[i].charset; i++)
        if (stricmp(faces[i].charset, charset->name) == 0) {
            faceStr = faces[i].face;
            break;
        }
    kchdebug("%s...", faceStr);
    QRegExp rexp(faceStr, FALSE, TRUE);
    if (face.contains(rexp)) {
        kchdebug("Yes, it is\n");
        return TRUE;
    }
    kchdebug("No, it isn't\n");
    return FALSE;
}

const KCharsetEntry *KCharsetsData::charsetOfFace(const QString &face)
{
    kchdebug("Searching for charset for face %s...\n", (const char *)face);
    for (int i = 0; faces[i].charset; i++) {
        const char *faceStr = faces[i].face;
        const char *key = faces[i].charset;
        kchdebug("testing if it is %s (%s)...", key, faceStr);
        QRegExp rexp(faceStr, FALSE, TRUE);
        kchdebug("regexp: %s face: %s\n", rexp.pattern(), (const char *)face);
        if (face.contains(rexp)) {
            kchdebug("Yes, it is\n");
            return charsetEntry(key);
        }
        kchdebug("No, it isn't\n");
    }
    return 0;
}

const char *KCharsetsData::faceForCharset(const KCharsetEntry *charset)
{
    for (int i = 0; faces[i].charset; i++)
        if (stricmp(faces[i].charset, charset->name) == 0) {
            return faces[i].face;
        }

    return 0;
}
