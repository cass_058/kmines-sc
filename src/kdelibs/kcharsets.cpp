/* This file is part of the KDE libraries
    Copyright (C) 1997 Jacek Konieczny (jajcus@zeus.polsl.gliwice.pl)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
    */

#include "kcharsets.h"
#include "kapp.h"
#include "kcharsetsdata.h"

#include <qfontinfo.h>
#include <qregexp.h>
#include <qstrlist.h>

KCharsetsData *KCharsets::data = 0;
KCharsetsData *KCharset::data = 0;
KCharsets *KCharset::charsets = 0;
uint KCharsets::count = 0;

/////////////////////////////////////////////////////////////////

KCharset::KCharset()
{
    if (!data || !charsets) {
        fatal("KCharset constructor called when no KCharsets object created");
        return;
    }
    entry = 0;
}

KCharset::KCharset(const KCharsetEntry *e)
{
    if (!data || !charsets) {
        fatal("KCharset constructor called when no KCharsets object created");
        return;
    }
    entry = e;
}

KCharset::KCharset(const char *str)
{
    if (!data || !charsets) {
        fatal("KCharset constructor called when no KCharsets object created");
        return;
    }
    entry = data->charsetEntry(str);
}

KCharset::KCharset(const QString s)
{
    if (!data || !charsets) {
        fatal("KCharset constructor called when no KCharsets object created");
        return;
    }
    entry = data->charsetEntry(s);
}

KCharset::KCharset(QFont::CharSet qtCharset)
{
    if (!data || !charsets) {
        fatal("KCharset constructor called when no KCharsets object created");
        return;
    }
    entry = data->charsetEntry(qtCharset);
}

KCharset::KCharset(const KCharset &kc)
{
    if (!data || !charsets) {
        fatal("KCharset copy constructor called when no KCharsets object created (?)");
        return;
    }
    entry = kc.entry;
}

KCharset &KCharset::operator=(const KCharset &kc)
{
    entry = kc.entry;
    return *this;
}

const char *KCharset::name() const
{
    if (entry)
        return entry->name;
    else
        return "unknown";
}

bool KCharset::isRegistered() const
{
    if (!entry)
        return FALSE;
    if (entry->registered)
        return TRUE;
    else
        return FALSE;
}

QFont::CharSet KCharset::qtCharset() const
{
    if (!entry) {
        warning("KCharset: Wrong charset!\n");
        return QFont::AnyCharSet;
    }
    if (!stricmp(name(), "any"))
        return QFont::AnyCharSet;
    if (entry)
        return entry->qtCharset;
    return QFont::AnyCharSet;
}

int KCharset::bits() const
{
    if (!entry) {
        warning("KCharset: Wrong charset!\n");
        return 8;
    }
    if (stricmp(name(), "unicode") == 0)
        return 16;
    else if (stricmp(name(), "iso-10640") == 0)
        return 16;
    else if (stricmp(name(), "us-ascii") == 0)
        return 7;
    else if (stricmp(name(), "unicode-1-1-utf-8") == 0)
        return 8;
    else if (stricmp(name(), "unicode-1-1-utf-7") == 0)
        return 7;
    else
        return 8;
}

QFont &KCharset::setQFont(QFont &fnt)
{
    if (!entry) {
        warning("KCharset: Wrong charset!\n");
        return fnt;
    }
    if ((stricmp(charsets->name(fnt), name()) == 0) || data->charsetOfFace(entry, fnt.family()))
        return fnt;

    kchdebug("setQFont: Setting font to: \"%s\"\n", name());
    QString faceStr = data->faceForCharset(entry);

    /* If Qt doesn't support this charset we must use the hack */
    if (qtCharset() == QFont::AnyCharSet && faceStr) {
        kchdebug("setQFont: Face for font: \"%s\"\n", (const char *)faceStr);
        faceStr.replace("\\*", fnt.family());
        kchdebug("setQFont: New face for font: \"%s\"\n", (const char *)faceStr);
        fnt.setCharSet(QFont::AnyCharSet);
        fnt.setFamily(faceStr);
        QFontInfo fi(fnt);
        if (fi.family() != faceStr) // hack doesn't work.
            // Maybe we know a face wich will work
            if (entry->good_family && !(entry->good_family->isEmpty())) {
                kchdebug("trying to find replacement font\n");
                QFontInfo fi(fnt);
                QString search;
                if (!fi.fixedPitch())
                    search += "-p";
                search += "-s"; // prefer scalable fonts
                if (fi.bold())
                    search += "-s";
                if (fi.italic())
                    search += "-i";

                while (!search.isEmpty()) {
                    int pos;
                    if ((pos = entry->good_family->find(search)) != -1) {
                        int start = entry->good_family->findRev("/", pos);
                        QString face = entry->good_family->mid(start + 1, pos - start - 1);
                        kchdebug("replacement: %s\n", (const char *)face);
                        fnt.setFamily(face);
                        break;
                    }
                    search.truncate(search.length() - 2);
                }
                if (search.isEmpty()) {
                    QString face = entry->good_family->left(entry->good_family->find("/") - 1);
                    kchdebug("replacement: %s\n", (const char *)face);
                    fnt.setFamily(face);
                }
            }
    } else {
        kchdebug("setQFont: trying to set charset to %i (family = %s\n", (int)qtCharset(), fnt.family());
        fnt.setCharSet(qtCharset());
        QFontInfo fi(fnt);
        int ch = fi.charSet();
        kchdebug("setQFont: got charset %i\n", ch);
        if (ch == QFont::AnyCharSet)
            ch = QFont::Latin1; // small hack...
        if (ch != qtCharset() && qtCharset() != QFont::AnyCharSet) {
            //#define kchdebug printf
            kchdebug("setQFont: didn't get charset: %d <--> %d\n", ch, qtCharset());
            if (entry->good_family && !(entry->good_family->isEmpty())) {
                kchdebug("trying to find replacement font\n");
                QFontInfo fi(fnt);
                QString search;
                if (!fi.fixedPitch())
                    search += "-p";
                search += "-s"; // prefer scalable fonts
                if (fi.bold())
                    search += "-s";
                if (fi.italic())
                    search += "-i";
                search += "/";

                while (search.length() > 1) {
                    int pos;
                    if ((pos = entry->good_family->find(search)) != -1) {
                        int start = entry->good_family->findRev("/", pos);
                        QString face = entry->good_family->mid(start + 1, pos - start - 1);
                        kchdebug("replacement: %s\n", (const char *)face);
                        fnt.setFamily(face);
                        break;
                    }
                    search.truncate(search.length() - 3);
                    search += "/";
                }
                if (search.length() == 1) {
                    QString face = entry->good_family->left(entry->good_family->find("/") - 1);
                    kchdebug("last replacement: %s\n", (const char *)face);
                    fnt.setFamily(face);
                }
#undef kchdebug
            } else if (faceStr) { /* nothing else works - we must use the hack */
                kchdebug("setQFont: Face for font: \"%s\"\n", (const char *)faceStr);
                faceStr.replace("\\*", fnt.family());
                kchdebug("setQFont: New face for font: \"%s\"\n", (const char *)faceStr);
                fnt.setCharSet(QFont::AnyCharSet);
                fnt.setFamily(faceStr);
            }
        }
    }
    kchdebug("setQFont: New charset: \"%s\"\n", charsets->name(fnt));
    return fnt;
}

KCharset::operator const KCharsetEntry *() const
{
    return entry;
}

/////////////////////////////////////////////////////////////////////
KCharsets::KCharsets()
{
    if (!data) {
        data = new KCharsetsData();
        KCharset::data = data;
        KCharset::charsets = this;
        count++;
    }
}

KCharsets::~KCharsets()
{
    if (!--count)
        delete data;
}

KCharset KCharsets::defaultCharset() const
{
    return defaultCh();
}

KCharset KCharsets::defaultCh() const
{
    return data->defaultCharset();
}

bool KCharsets::setDefault(KCharset ch)
{
    if (ch.ok()) {
        data->setDefaultCharset(ch.entry);
        return TRUE;
    }
    warning("Wrong charset (%s)! Setting to default (us-ascii)", ch.name());
    const KCharsetEntry *ce = data->charsetEntry("us-ascii");
    data->setDefaultCharset(ce);
    return FALSE;
}

QStrList KCharsets::available() const
{
    QStrList w;
    int i;
    for (i = 0; data->charsetEntry(i); i++)
        w.append(data->charsetEntry(i)->name);
    return w;
}

QStrList KCharsets::registered() const
{
    QStrList w;
    int i;
    for (i = 0; data->charsetEntry(i); i++)
        if (data->charsetEntry(i)->registered)
            w.append(data->charsetEntry(i)->name);
    return w;
}

bool KCharsets::isAvailable(KCharset charset)
{
    return charset.isAvailable();
}

bool KCharsets::isRegistered(KCharset charset)
{
    return charset.isRegistered();
}

int KCharsets::bits(KCharset charset)
{
    return charset.bits();
}

const char *KCharsets::name(QFont::CharSet qtcharset)
{
    if (qtcharset == QFont::AnyCharSet)
        return "unknown";
    return KCharset(qtcharset);
}

KCharset KCharsets::charset(QFont::CharSet qtcharset)
{
    return KCharset(qtcharset);
}

QFont::CharSet KCharsets::qtCharset()
{
    return qtCharset(data->defaultCharset());
}

QFont::CharSet KCharsets::qtCharset(KCharset set)
{
    return set.qtCharset();
}

QFont &KCharsets::setQFont(QFont &fnt, KCharset charset)
{
    return charset.setQFont(fnt);
}

QFont &KCharsets::setQFont(QFont &fnt)
{
    return KCharset(data->defaultCharset()).setQFont(fnt);
}

KCharset KCharsets::charset(const QFont &font)
{
    kchdebug("Testing charset of font: %s, qtcharset=%i\n", font.family(), (int)font.charSet());
    if (font.charSet() != QFont::AnyCharSet)
        return charset(font.charSet());
    const KCharsetEntry *ce = data->charsetOfFace(font.family());
    kchdebug("ce=%p ce->name=%s\n", ce, ce ? ce->name : 0);
    return KCharset(ce);
}

const char *KCharsets::name(const QFont &font)
{
    return charset(font);
}
