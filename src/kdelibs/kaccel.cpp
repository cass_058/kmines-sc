/*
    Copyright (C) 1998 Mark Donohoe <donohoe@kde.org>
    Copyright (C) 1997 Nicolas Hadacek <hadacek@via.ecp.fr>
    Copyright (C) 1998 Matthias Ettrich <ettrich@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include "kaccel.h"
#include "kapp.h"
#include "kckey.h"

#include <qapplication.h>
#include <qdrawutil.h>
#include <qkeycode.h>
#include <qlayout.h>
#include <qmessagebox.h>
#include <qpainter.h>

KAccel::KAccel(QWidget *parent, const char *name)
    : aKeyDict(100)
{
    aAvailableId = 1;
    bEnabled = true;
    aGroup = "Keys";
    pAccel = new QAccel(parent, name);
}

KAccel::~KAccel()
{
    delete pAccel;
}

void KAccel::clear()
{
    pAccel->clear();
    aKeyDict.clear();
}

void KAccel::connectItem(const char *action, const QObject *receiver, const char *member, bool activate)
{
    if (!action)
        return;
    KKeyEntry *pEntry = aKeyDict[action];

    if (!pEntry) {
        QString str;
        str.sprintf("KAccel : Cannot connect action %s ", action);
        str.append("which is not in the object dictionary");
        warning(str);
        return;
    }

    pEntry->receiver = receiver;
    pEntry->member = member;
    pEntry->aAccelId = aAvailableId;
    aAvailableId++;

    pAccel->insertItem(pEntry->aCurrentKeyCode, pEntry->aAccelId);
    pAccel->connectItem(pEntry->aAccelId, receiver, member);

    if (!activate)
        setItemEnabled(action, FALSE);
}

uint KAccel::count() const
{
    return aKeyDict.count();
}

uint KAccel::currentKey(const char *action)
{
    KKeyEntry *pEntry = aKeyDict[action];

    if (!pEntry)
        return 0;
    else
        return pEntry->aCurrentKeyCode;
}

const char *KAccel::description(const char *action)
{
    KKeyEntry *pEntry = aKeyDict[action];

    if (!pEntry)
        return 0;
    else
        return pEntry->descr;
}

uint KAccel::defaultKey(const char *action)
{
    KKeyEntry *pEntry = aKeyDict[action];

    if (!pEntry)
        return 0;
    else
        return pEntry->aDefaultKeyCode;
}

void KAccel::disconnectItem(const char *action, const QObject *receiver, const char *member)
{
    KKeyEntry *pEntry = aKeyDict[action];
    if (!pEntry)
        return;

    pAccel->disconnectItem(pEntry->aAccelId, receiver, member);
}

const char *KAccel::findKey(int key) const
{
    QDictIterator<KKeyEntry> aKeyIt(aKeyDict);
    aKeyIt.toFirst();
    KKeyEntry *pE;
    while (aKeyIt.current()) {
        pE = aKeyIt.current();
        if ((unsigned int)key == pE->aCurrentKeyCode)
            return aKeyIt.currentKey();
        ++aKeyIt;
    }
    return 0;
}

bool KAccel::insertItem(const char *descr, const char *action, uint keyCode, bool configurable)
{
    KKeyEntry *pEntry = aKeyDict[action];

    if (pEntry)
        removeItem(action);

    pEntry = new KKeyEntry;
    aKeyDict.insert(action, pEntry);

    pEntry->aDefaultKeyCode = keyCode;
    pEntry->aCurrentKeyCode = keyCode;
    pEntry->aConfigKeyCode = keyCode;
    pEntry->bConfigurable = configurable;
    pEntry->aAccelId = 0;
    pEntry->receiver = 0;
    pEntry->member = 0;
    pEntry->descr = descr;

    return TRUE;
}

bool KAccel::insertItem(const char *descr, const char *action, const char *keyCode, bool configurable)
{
    uint iKeyCode = stringToKey(keyCode);
    return insertItem(descr, action, iKeyCode, configurable);
}

bool KAccel::insertItem(const char *action, uint keyCode, bool configurable)
{
    return insertItem(action, action, keyCode, configurable);
}

void KAccel::changeMenuAccel(QPopupMenu *menu, int id, const char *action)
{
    QString s = menu->text(id);
    if (!s)
        return;
    if (!action)
        return;

    int i = s.find('\t');

    QString k = keyToString(currentKey(action), true);
    if (!k)
        return;

    if (i >= 0)
        s.replace(i + 1, s.length() - i, k);
    else {
        s += '\t';
        s += k;
    }

    QPixmap *pp = menu->pixmap(id);
    if (pp && !pp->isNull())
        menu->changeItem(*pp, s, id);
    else
        menu->changeItem(s, id);
}

bool KAccel::isEnabled()
{
    return bEnabled;
}

bool KAccel::isItemEnabled(const char *action)
{
    KKeyEntry *pEntry = aKeyDict[action];

    if (!pEntry)
        return false;
    else
        return pEntry->bEnabled;
}

QDict<KKeyEntry> KAccel::keyDict()
{
    return aKeyDict;
}

void KAccel::readSettings(KConfig *config)
{
    KConfig *pConfig = config ? config : kapp->getConfig();
    pConfig->setGroup(aGroup.data());

    QDictIterator<KKeyEntry> aKeyIt(aKeyDict);
    aKeyIt.toFirst();
    KKeyEntry *pE;
    QString s;
    while (aKeyIt.current()) {
        pE = aKeyIt.current();
        s = pConfig->readEntry(aKeyIt.currentKey());

        if (s.isNull())
            pE->aConfigKeyCode = pE->aDefaultKeyCode;
        else
            pE->aConfigKeyCode = stringToKey(s.data());

        pE->aCurrentKeyCode = pE->aConfigKeyCode;
        if (pE->aAccelId && pE->aCurrentKeyCode) {
            pAccel->disconnectItem(pE->aAccelId, pE->receiver, pE->member);
            pAccel->removeItem(pE->aAccelId);
            pAccel->insertItem(pE->aCurrentKeyCode, pE->aAccelId);
            pAccel->connectItem(pE->aAccelId, pE->receiver, pE->member);
        }
        ++aKeyIt;
    }
}

void KAccel::removeItem(const char *action)
{
    KKeyEntry *pEntry = aKeyDict[action];

    if (!pEntry)
        return;

    if (pEntry->aAccelId) {
        pAccel->disconnectItem(pEntry->aAccelId, pEntry->receiver, pEntry->member);
        pAccel->removeItem(pEntry->aAccelId);
    }

    aKeyDict.remove(action);
}

void KAccel::setEnabled(bool activate)
{
    QDictIterator<KKeyEntry> aKeyIt(aKeyDict);
    aKeyIt.toFirst();
    while (aKeyIt.current()) {
        setItemEnabled(aKeyIt.currentKey(), activate);
        ++aKeyIt;
    }
    bEnabled = activate;
}

void KAccel::setItemEnabled(const char *action, bool activate)
{
    KKeyEntry *pEntry = aKeyDict[action];
    if (!pEntry) {
        QString str;
        str.sprintf(
            "KAccel : cannont enable action %s"
            "which is not in the object dictionary",
            action);
        warning(str);
        return;
    }

    pAccel->setItemEnabled(pEntry->aAccelId, activate);
}

bool KAccel::setKeyDict(QDict<KKeyEntry> nKeyDict)
{
    // Disconnect and remove all items in pAccel
    QDictIterator<KKeyEntry> *aKeyIt = new QDictIterator<KKeyEntry>(aKeyDict);
    aKeyIt->toFirst();
    KKeyEntry *pE;
    while (aKeyIt->current()) {
        pE = aKeyIt->current();
        if (pE->aAccelId && pE->aCurrentKeyCode) {
            pAccel->disconnectItem(pE->aAccelId, pE->receiver, pE->member);
            pAccel->removeItem(pE->aAccelId);
        }
        ++*aKeyIt;
    }

    // Clear the dictionary
    aKeyDict.clear();

    // Insert the new items into the dictionary and reconnect if neccessary
    // Note also swap config and current key codes !!!!!!
    delete aKeyIt; // tanghus
    aKeyIt = new QDictIterator<KKeyEntry>(nKeyDict);
    aKeyIt->toFirst();
    KKeyEntry *pEntry;
    while (aKeyIt->current()) {
        pEntry = new KKeyEntry;
        pE = aKeyIt->current();
        aKeyDict.insert(aKeyIt->currentKey(), pEntry);
        pEntry->aDefaultKeyCode = pE->aDefaultKeyCode;
        // Note we write config key code to current key code !!
        pEntry->aCurrentKeyCode = pE->aConfigKeyCode;
        pEntry->aConfigKeyCode = pE->aConfigKeyCode;
        pEntry->bConfigurable = pE->bConfigurable;
        pEntry->aAccelId = pE->aAccelId;
        pEntry->receiver = pE->receiver;
        pEntry->member = pE->member;
        pEntry->descr = pE->descr; // tanghus

        if (pEntry->aAccelId && pEntry->aCurrentKeyCode) {
            pAccel->insertItem(pEntry->aCurrentKeyCode, pEntry->aAccelId);
            pAccel->connectItem(pEntry->aAccelId, pEntry->receiver, pEntry->member);
        }
        ++*aKeyIt;
    }

    delete aKeyIt; // tanghus
    return true;
}

void KAccel::setConfigGroup(const char *group)
{
    aGroup = group;
}

const char *KAccel::configGroup()
{
    return aGroup.data();
}

void KAccel::writeSettings(KConfig *config)
{
    KConfig *pConfig = config ? config : kapp->getConfig();
    pConfig->setGroup(aGroup.data());

    QDictIterator<KKeyEntry> aKeyIt(aKeyDict);
    aKeyIt.toFirst();
    KKeyEntry *pE;
    while (aKeyIt.current()) {
        pE = aKeyIt.current();
        if (pE->bConfigurable) {
            pConfig->writeEntry(aKeyIt.currentKey(), keyToString(pE->aCurrentKeyCode));
        }
        ++aKeyIt;
    }
    pConfig->sync();
}

/*****************************************************************************/

const QString keyToString(uint keyCode, bool i18_n)
{
    QString res;

    if (keyCode == 0) {
        res = "";
        return res;
    }
    if (!i18_n) {
        if (keyCode & SHIFT) {
            res += ("SHIFT");
            res += "+";
        }
        if (keyCode & CTRL) {
            res += ("CTRL");
            res += "+";
        }
        if (keyCode & ALT) {
            res += ("ALT");
            res += "+";
        }
    } else {
        if (keyCode & SHIFT) {
            res = i18n("SHIFT");
            res += "+";
        }
        if (keyCode & CTRL) {
            res += i18n("CTRL");
            res += "+";
        }
        if (keyCode & ALT) {
            res += i18n("ALT");
            res += "+";
        }
    }

    uint kCode = keyCode & ~(SHIFT | CTRL | ALT);

    for (int i = 0; i < NB_KEYS; i++) {
        if (kCode == (uint)KKeys[i].code) {
            res += KKeys[i].name;
            return res;
        }
    }

    return 0;
}

uint stringToKey(const char *key)
{
    char *toks[4], *next_tok;
    uint keyCode = 0;
    int j, nb_toks = 0;
    char sKey[200];

    // printf("string to key %s\n", key);
    if (key == 0) {
        warning("stringToKey::Null key");
        return 0;
    }
    if (strcmp(key, "") == -1) {
        warning("stringToKey::Empty key");
        return 0;
    }

    strncpy(sKey, (const char *)key, sizeof sKey - 1);
    next_tok = strtok(sKey, "+");

    if (next_tok == 0L)
        return 0;

    do {
        toks[nb_toks] = next_tok;
        nb_toks++;
        if (nb_toks == 5)
            return 0;
        next_tok = strtok(0L, "+");
    } while (next_tok != 0L);

    /* we test if there is one and only one key (the others tokens
       are accelerators); we also fill the keycode with infos */
    bool keyFound = FALSE;
    for (int i = 0; i < nb_toks; i++) {
        if (strcmp(toks[i], "SHIFT") == 0)
            keyCode |= SHIFT;
        else if (strcmp(toks[i], "CTRL") == 0)
            keyCode |= CTRL;
        else if (strcmp(toks[i], "ALT") == 0)
            keyCode |= ALT;
        else if (strcmp(toks[i], "Umschalt") == 0)
            keyCode |= SHIFT;
        else if (strcmp(toks[i], "Strg") == 0)
            keyCode |= CTRL;
        else if (strcmp(toks[i], "Alt") == 0)
            keyCode |= ALT;
        else if (strcmp(toks[i], i18n("SHIFT")) == 0)
            keyCode |= SHIFT;
        else if (strcmp(toks[i], i18n("CTRL")) == 0)
            keyCode |= CTRL;
        else if (strcmp(toks[i], i18n("ALT")) == 0)
            keyCode |= ALT;
        else {
            /* key already found ? */
            if (keyFound)
                return 0;
            keyFound = TRUE;

            /* search key */
            for (j = 0; j < NB_KEYS; j++) {
                if (strcmp(toks[i], KKeys[j].name) == 0) {
                    keyCode |= KKeys[j].code;
                    break;
                }
            }

            /* key name ot found ? */
            if (j == NB_KEYS)
                return 0;
        }
    }

    return keyCode;
}
