/* This file is part of the KDE libraries
    Copyright (C) 1997 Matthias Kalle Dalheimer (kalle@kde.org)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include "kconfigbase.h"
#include "kapp.h"
#include "kcharsets.h"

#include <qfile.h>
#include <qtextstream.h>

static QString printableToString(const QString &s)
{
    if (!s.contains('\\'))
        return s;
    QString result = "";
    unsigned int i = 0;
    if (s.length() > 1) { // remember: s.length() is unsigned....
        for (i = 0; i < s.length() - 1; i++) {
            if (s[i] == '\\') {
                i++;
                if (s[i] == '\\')
                    result.insert(result.length(), s[i]);
                else if (s[i] == 'n')
                    result.append("\n");
                else if (s[i] == 't')
                    result.append("\t");
                else if (s[i] == 'r')
                    result.append("\r");
                else {
                    result.append("\\");
                    result.insert(result.length(), s[i]);
                }
            } else
                result.insert(result.length(), s[i]);
        }
    }
    if (i < s.length())
        result.insert(result.length(), s[i]);
    return result;
}

KConfigBase::KConfigBase()
{
    pData = new KConfigBaseData();

    // setup a group entry for the default group
    KEntryDict *pDefGroup = new KEntryDict(37, false);
    pDefGroup->setAutoDelete(true);
    data()->aGroupDict.insert("<default>", pDefGroup);
}

KConfigBase::~KConfigBase()
{
    delete pData;
}

void KConfigBase::parseOneConfigFile(QFile &rFile, KGroupDict *pWriteBackDict)
{
    if (!rFile.isOpen()) // come back, if you have real work for us ;->
        return;

    QString aCurrentLine;
    QString aCurrentGroup = "";

    QDict<KEntryDict> *pDict;
    if (pWriteBackDict)
        // write back mode - don't mess up the normal dictionary
        pDict = pWriteBackDict;
    else
        // normal mode - use the normal dictionary
        pDict = &(data()->aGroupDict);

    KEntryDict *pCurrentGroupDict = (*pDict)["<default>"];

    // reset the stream's device
    rFile.at(0);
    QTextStream aStream(&rFile);
    while (!aStream.eof()) {
        aCurrentLine = aStream.readLine();

        // check for a group
        int nLeftBracket = aCurrentLine.find('[');
        int nRightBracket = aCurrentLine.find(']', 1);
        if (nLeftBracket == 0 && nRightBracket != -1) {
            // group found; get the group name by taking everything in
            // between the brackets
            aCurrentGroup = aCurrentLine.mid(1, nRightBracket - 1);

            // check if there already is such a group in the group
            // dictionary
            pCurrentGroupDict = (*pDict)[aCurrentGroup];
            if (!pCurrentGroupDict) {
                // no such group -> create a new entry dictionary
                KEntryDict *pNewDict = new KEntryDict(37, false);
                pNewDict->setAutoDelete(true);
                (*pDict).insert(aCurrentGroup, pNewDict);

                // this is now the current group
                pCurrentGroupDict = pNewDict;
            }
            continue;
        };

        if (aCurrentLine[0] == '#')
            // comment character in the first column, skip the line
            continue;

        int nEqualsPos = aCurrentLine.find('=');
        if (nEqualsPos == -1)
            // no equals sign: incorrect or empty line, skip it
            continue;

        // insert the key/value line into the current dictionary
        KEntryDictEntry *pEntry = new KEntryDictEntry;
        pEntry->aValue = printableToString(aCurrentLine.right(aCurrentLine.length() - nEqualsPos - 1)).stripWhiteSpace();
        pEntry->bDirty = false;

        pCurrentGroupDict->insert(aCurrentLine.left(nEqualsPos).stripWhiteSpace(), pEntry);
    }
}

void KConfigBase::setGroup(const char *pGroup)
{
    if (!pGroup)
        data()->aGroup = "<default>";
    else
        data()->aGroup = pGroup;
}

const char *KConfigBase::group() const
{
    static QString aEmptyStr = "";
    if (data()->aGroup == "<default>")
        return aEmptyStr;
    else
        return data()->aGroup;
}

const QString KConfigBase::readEntry(const char *pKey, const char *pDefault) const
{
    QString aValue;
    // retrieve the current group dictionary
    KEntryDict *pCurrentGroupDict = data()->aGroupDict[data()->aGroup.data()];

    if (pCurrentGroupDict) {
        // find the value for the key in the current group
        KEntryDictEntry *pEntryData = (*pCurrentGroupDict)[pKey];

        if (pEntryData)
            aValue = pEntryData->aValue;
        else if (pDefault)
            aValue = pDefault;
    } else if (pDefault)
        aValue = pDefault;

    return aValue;
}

int KConfigBase::readNumEntry(const char *pKey, int nDefault) const
{
    bool ok;
    int rc;

    QString aValue = readEntry(pKey);
    if (aValue.isNull())
        return nDefault;
    else if (aValue == "true")
        return 1;
    else if (aValue == "on")
        return 1;
    else {
        rc = aValue.toInt(&ok);
        return (ok ? rc : 0);
    }
}

bool KConfigBase::readBoolEntry(const char *pKey, const bool bDefault) const
{
    QString aValue = readEntry(pKey);
    if (aValue.isNull())
        return bDefault;
    else {
        if (aValue == "true")
            return true;
        else {
            bool bOK;
            int val = aValue.toInt(&bOK);
            if (bOK && val != 0)
                return true;
            else
                return false;
        }
    }
}

QFont KConfigBase::readFontEntry(const char *pKey, const QFont *pDefault) const
{
    QFont aRetFont;
    QString aValue = readEntry(pKey);
    if (!aValue.isNull()) {
        // find first part (font family)
        int nIndex = aValue.find(',');
        if (nIndex == -1) {
            if (pDefault)
                aRetFont = *pDefault;
            return aRetFont;
        }
        aRetFont.setFamily(aValue.left(nIndex));

        // find second part (point size)
        int nOldIndex = nIndex;
        nIndex = aValue.find(',', nOldIndex + 1);
        if (nIndex == -1) {
            if (pDefault)
                aRetFont = *pDefault;
            return aRetFont;
        }

        aRetFont.setPointSize(aValue.mid(nOldIndex + 1, nIndex - nOldIndex - 1).toInt());

        // find third part (style hint)
        nOldIndex = nIndex;
        nIndex = aValue.find(',', nOldIndex + 1);

        if (nIndex == -1) {
            if (pDefault)
                aRetFont = *pDefault;
            return aRetFont;
        }

        aRetFont.setStyleHint((QFont::StyleHint)aValue.mid(nOldIndex + 1, nIndex - nOldIndex - 1).toUInt());

        // find fourth part (char set)
        nOldIndex = nIndex;
        nIndex = aValue.find(',', nOldIndex + 1);

        if (nIndex == -1) {
            if (pDefault)
                aRetFont = *pDefault;
            return aRetFont;
        }

        QString chStr = aValue.mid(nOldIndex + 1, nIndex - nOldIndex - 1);
        bool chOldEntry;
        QFont::CharSet chId = (QFont::CharSet)aValue.mid(nOldIndex + 1, nIndex - nOldIndex - 1).toUInt(&chOldEntry);
        if (chOldEntry)
            aRetFont.setCharSet(chId);
        else if (kapp) {
            if (chStr == "default") {
                if (kapp->localeConstructed())
                    chStr = klocale->charset();
                else
                    chStr = "iso-8859-1";
            }
            kapp->getCharsets()->setQFont(aRetFont, chStr);
        }

        // find fifth part (weight)
        nOldIndex = nIndex;
        nIndex = aValue.find(',', nOldIndex + 1);

        if (nIndex == -1) {
            if (pDefault)
                aRetFont = *pDefault;
            return aRetFont;
        }

        aRetFont.setWeight(aValue.mid(nOldIndex + 1, nIndex - nOldIndex - 1).toUInt());

        // find sixth part (font bits)
        uint nFontBits = aValue.right(aValue.length() - nIndex - 1).toUInt();
        if (nFontBits & 0x01)
            aRetFont.setItalic(true);
        else
            aRetFont.setItalic(false);

        if (nFontBits & 0x02)
            aRetFont.setUnderline(true);
        else
            aRetFont.setUnderline(false);

        if (nFontBits & 0x04)
            aRetFont.setStrikeOut(true);
        else
            aRetFont.setStrikeOut(false);

        if (nFontBits & 0x08)
            aRetFont.setFixedPitch(true);
        else
            aRetFont.setFixedPitch(false);

        if (nFontBits & 0x20)
            aRetFont.setRawMode(true);
        else
            aRetFont.setRawMode(false);
    } else {
        if (pDefault)
            aRetFont = *pDefault;
    }

    return aRetFont;
}

QColor KConfigBase::readColorEntry(const char *pKey, const QColor *pDefault) const
{
    QColor aRetColor;
    int nRed = 0, nGreen = 0, nBlue = 0;

    QString aValue = readEntry(pKey);
    if (!aValue.isEmpty()) {
        if (aValue.left(1) == "#") {
            aRetColor.setNamedColor(aValue);
        } else {
            bool bOK;

            // find first part (red)
            int nIndex = aValue.find(',');

            if (nIndex == -1) {
                // return a sensible default -- Bernd
                if (pDefault)
                    aRetColor = *pDefault;
                return aRetColor;
            }

            nRed = aValue.left(nIndex).toInt(&bOK);

            // find second part (green)
            int nOldIndex = nIndex;
            nIndex = aValue.find(',', nOldIndex + 1);

            if (nIndex == -1) {
                // return a sensible default -- Bernd
                if (pDefault)
                    aRetColor = *pDefault;
                return aRetColor;
            }
            nGreen = aValue.mid(nOldIndex + 1, nIndex - nOldIndex - 1).toInt(&bOK);

            // find third part (blue)
            nBlue = aValue.right(aValue.length() - nIndex - 1).toInt(&bOK);

            aRetColor.setRgb(nRed, nGreen, nBlue);
        }
    } else {
        if (pDefault)
            aRetColor = *pDefault;
    }

    return aRetColor;
}

const char *KConfigBase::writeEntry(const char *pKey, const char *pValue, bool bPersistent)
{
    QString aValue;

    // retrieve the current group dictionary
    KEntryDict *pCurrentGroupDict = data()->aGroupDict[data()->aGroup.data()];

    if (!pCurrentGroupDict) {
        // no such group -> create a new entry dictionary
        KEntryDict *pNewDict = new KEntryDict(37, false);
        pNewDict->setAutoDelete(true);
        data()->aGroupDict.insert(data()->aGroup.data(), pNewDict);

        // this is now the current group
        pCurrentGroupDict = pNewDict;
    }

    // try to retrieve the current entry for this key
    KEntryDictEntry *pEntryData = (*pCurrentGroupDict)[pKey];
    if (pEntryData) {
        // there already is such a key
        aValue = pEntryData->aValue; // save old key as return value
        pEntryData->aValue = pValue; // set new value
        if (bPersistent)
            pEntryData->bDirty = TRUE;
    } else {
        // the key currently does not exist
        KEntryDictEntry *pEntry = new KEntryDictEntry;
        pEntry->aValue = pValue;
        if (bPersistent)
            pEntry->bDirty = TRUE;

        // insert the new entry into group dictionary
        pCurrentGroupDict->insert(pKey, pEntry);
    }

    // the KConfig object is dirty now
    if (bPersistent)
        data()->bDirty = true;
    return aValue;
}

const char *KConfigBase::writeEntry(const char *pKey, int nValue, bool bPersistent)
{
    QString aValue;

    aValue.setNum(nValue);

    return writeEntry(pKey, aValue, bPersistent);
}

const char *KConfigBase::writeEntry(const char *pKey, bool bValue, bool bPersistent)
{
    QString aValue;

    if (bValue)
        aValue = "true";
    else
        aValue = "false";

    return writeEntry(pKey, aValue, bPersistent);
}

const char *KConfigBase::writeEntry(const char *pKey, const QFont &rFont, bool bPersistent)
{
    QString aValue;
    QString aCharset;
    UINT8 nFontBits = 0;
    // this mimics get_font_bits() from qfont.cpp
    if (rFont.italic())
        nFontBits = nFontBits | 0x01;
    if (rFont.underline())
        nFontBits = nFontBits | 0x02;
    if (rFont.strikeOut())
        nFontBits = nFontBits | 0x04;
    if (rFont.fixedPitch())
        nFontBits = nFontBits | 0x08;
    if (rFont.rawMode())
        nFontBits = nFontBits | 0x20;

    if (kapp) {
        aCharset = kapp->getCharsets()->name(rFont);
    } else
        aCharset = "default";
    aValue.sprintf("%s,%d,%d,%s,%d,%d", rFont.family(), rFont.pointSize(), rFont.styleHint(), (const char *)aCharset, rFont.weight(), nFontBits);

    return writeEntry(pKey, aValue, bPersistent);
}

void KConfigBase::writeEntry(const char *pKey, const QColor &rColor, bool bPersistent)
{
    QString aValue;
    aValue.sprintf("%d,%d,%d", rColor.red(), rColor.green(), rColor.blue());

    writeEntry(pKey, aValue, bPersistent);
}

void KConfigBase::rollback(bool bDeep)
{
    // clear the global bDirty flag in all cases
    data()->bDirty = false;

    // if bDeep is true, clear the bDirty flags of all the entries
    if (!bDeep)
        return;
    QDictIterator<KEntryDict> aIt(data()->aGroupDict);
    // loop over all the groups
    const char *pCurrentGroup;
    while ((pCurrentGroup = aIt.currentKey())) {
        QDictIterator<KEntryDictEntry> aInnerIt(*aIt.current());
        // loop over all the entries
        KEntryDictEntry *pCurrentEntry;
        while ((pCurrentEntry = aInnerIt.current())) {
            pCurrentEntry->bDirty = false;
            ++aInnerIt;
        }
        ++aIt;
    }
}

bool KConfigBase::hasKey(const char *pKey) const
{
    QString aValue = readEntry(pKey);
    return !aValue.isNull();
}

KEntryIterator *KConfigBase::entryIterator(const char *pGroup)
{
    // find the group
    KEntryDict *pCurrentGroupDict = data()->aGroupDict[pGroup];

    if (!pCurrentGroupDict)
        return 0L; // that group does not exist

    return new KEntryIterator(*pCurrentGroupDict);
}

void KConfigBase::reparseConfiguration()
{
    data()->aGroupDict.clear();

    // setup a group entry for the default group
    KEntryDict *pDefGroup = new KEntryDict(37, false);
    pDefGroup->setAutoDelete(true);
    data()->aGroupDict.insert("<default>", pDefGroup);

    // the next three lines are indeed important.
    // no longer (Kalle, 04/10/97)
    //  data()->pAppStream->device()->close();
    //  if (!data()->pAppStream->device()->open( IO_ReadWrite ))
    //    data()->pAppStream->device()->open( IO_ReadOnly );

    parseConfigFiles();
}
