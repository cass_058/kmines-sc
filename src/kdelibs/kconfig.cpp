/* This file is part of the KDE libraries
    Copyright (C) 1997 Matthias Kalle Dalheimer (kalle@kde.org)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#include "kconfig.h"
#include "kapp.h"

#include <qfileinfo.h>
#include <qtextstream.h>

static QString stringToPrintable(const QString &s)
{
    QString result;
    unsigned int i;
    for (i = 0; i < s.length(); i++) {
        if (s[i] == '\n')
            result.append("\\n");
        else if (s[i] == '\t')
            result.append("\\t");
        else if (s[i] == '\r')
            result.append("\\r");
        else if (s[i] == '\\')
            result.append("\\\\");
        else
            result.insert(result.length(), s[i]);
    }
    return result;
}

KConfig::KConfig(const char *pFile)
{
    if (pFile) {
        data()->aAppFile = pFile;
        // the file should exist in any case
        QFileInfo info(pFile);
        if (!info.exists()) {
            QFile file(pFile);
            file.open(IO_WriteOnly);
            file.close();
        }
    }

    parseConfigFiles();
}

KConfig::~KConfig()
{
    sync();
}

void KConfig::parseConfigFiles()
{
    // Parse the application config file if available
    if (!data()->aAppFile.isEmpty()) {
        QFile aConfigFile(data()->aAppFile);
        // we can already be sure that this file exists
        /* Actually, we can't: CHange by Alex */
        if (!aConfigFile.open(IO_ReadOnly)) {
            QString tmp = data()->aAppFile.copy();
            data()->aAppFile.sprintf("%s/%s", KApplication::localconfigdir().data(), tmp.data());
            aConfigFile.close();
            aConfigFile.setName(data()->aAppFile.data());
            aConfigFile.open(IO_ReadOnly);
        }

        parseOneConfigFile(aConfigFile, 0L);
        aConfigFile.close();
    }
}

bool KConfig::writeConfigFile(QFile &rConfigFile)
{
    // create a temporary dictionary that represents the file to be written
    QDict<KEntryDict> aTempDict(37, FALSE);
    aTempDict.setAutoDelete(true);

    // setup a group entry for the default group
    KEntryDict *pDefGroup = new KEntryDict(37, false);
    pDefGroup->setAutoDelete(true);
    aTempDict.insert("<default>", pDefGroup);

    // fill the temporary structure with entries from the file
    parseOneConfigFile(rConfigFile, &aTempDict);

    // augment this structure with the dirty entries from the normal structure
    QDictIterator<KEntryDict> aIt(data()->aGroupDict);

    // loop over all the groups
    const char *pCurrentGroup;
    while ((pCurrentGroup = aIt.currentKey())) {
        QDictIterator<KEntryDictEntry> aInnerIt(*aIt.current());
        // loop over all the entries
        KEntryDictEntry *pCurrentEntry;
        while ((pCurrentEntry = aInnerIt.current())) {
            if (pCurrentEntry->bDirty) {
                // enter the *aInnerIt.currentKey()/pCurrentEntry->aValue
                // pair into group *pCurrentGroup in aTempDict
                KEntryDict *pTempGroup;
                if (!(pTempGroup = aTempDict[pCurrentGroup])) {
                    // group does not exist in aTempDict
                    pTempGroup = new KEntryDict(37, false);
                    pTempGroup->setAutoDelete(true);
                    aTempDict.insert(pCurrentGroup, pTempGroup);
                }
                KEntryDictEntry *pNewEntry = new KEntryDictEntry();
                pNewEntry->aValue = pCurrentEntry->aValue;
                pNewEntry->bDirty = false;
                pTempGroup->replace(aInnerIt.currentKey(), pNewEntry);
            }
            ++aInnerIt;
        }
        ++aIt;
    }
    // truncate file
    rConfigFile.close();

    rConfigFile.open(IO_Truncate | IO_WriteOnly);
    QTextStream *pStream = new QTextStream(&rConfigFile);

    // write back -- start with the default group
    KEntryDict *pDefWriteGroup = aTempDict["<default>"];
    if (pDefWriteGroup) {
        QDictIterator<KEntryDictEntry> aWriteInnerIt(*pDefWriteGroup);
        while (aWriteInnerIt.current()) {
            *pStream << aWriteInnerIt.currentKey() << "=" << stringToPrintable(aWriteInnerIt.current()->aValue) << '\n';
            ++aWriteInnerIt;
        }
    }

    QDictIterator<KEntryDict> aWriteIt(aTempDict);
    while (aWriteIt.current()) {
        // check if it's not the default group (which has already been written)
        if (strcmp(aWriteIt.currentKey(), "<default>")) {
            *pStream << '[' << aWriteIt.currentKey() << ']' << '\n';
            QDictIterator<KEntryDictEntry> aWriteInnerIt(*aWriteIt.current());
            while (aWriteInnerIt.current()) {
                *pStream << aWriteInnerIt.currentKey() << "=" << stringToPrintable(aWriteInnerIt.current()->aValue) << '\n';
                ++aWriteInnerIt;
            }
        }
        ++aWriteIt;
    }

    // clean up
    delete pStream;
    rConfigFile.close();

    // Reopen the config file.
    rConfigFile.open(IO_ReadWrite);

    return false;
}

void KConfig::sync()
{
    // write-sync is only necessary if there are dirty entries
    if (data()->bDirty) {
        // write the file
        if (!data()->aAppFile.isEmpty()) {
            // is it writable?
            QFile aConfigFile(data()->aAppFile);
            aConfigFile.open(IO_ReadWrite);

            if (aConfigFile.isWritable())
                writeConfigFile(aConfigFile);
            aConfigFile.close();
        }
    }

    // no more dirty entries
    rollback();
}
