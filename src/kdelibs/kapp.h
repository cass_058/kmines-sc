/* This file is part of the KDE libraries
    Copyright (C) 1997 Matthias Kalle Dalheimer (kalle@kde.org)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#ifndef KAPP_H
#define KAPP_H

#include <kconfig.h>
#include <klocale.h>

#include <qapplication.h>
#include <qfile.h>
#include <qpixmap.h>
#include <qpopupmenu.h>
#include <qstrlist.h>

#ifndef klocale
#define klocale KApplication::getKApplication()->getLocale()
#endif

#ifndef i18n
#define i18n(X) KApplication::getKApplication()->getLocale()->translate(X)
#endif

#define kapp KApplication::getKApplication()

class KCharsets;

/**
 * A base class for all KDE applications.
 *
 * KApplication provides the application with KDE defaults such as
 * accelerators, common menu entries, a KConfig object
 * etc. KApplication installs a signal handler for the SIGCHLD signal
 * in order to avoid zombie children. If you want to catch this signal
 * yourself or don't want it to be caught at all, you have set a new
 * signal handler (or SIG_IGN) after KApplication's constructor has
 * run.
 *
 * @short A base class for all KDE applications.
 * @author Matthias Kalle Dalheimer <kalle@kde.org>
 * @version $Id: kapp.h,v 1.61.2.3 1999/09/06 14:11:43 kulow Exp $
 */
class KApplication : public QApplication
{
    Q_OBJECT
public:
    /**
     * Constructor. Pass command-line arguments.
     *
     * A KConfig object is
     * created that contains an application-specific config file whose
     * name is "~/." + argv[0] + "rc". This constructor should be considered
     * obsolete. The state of the application-specific config file may be
     * queried afterwards with getConfigState().
     */
    KApplication(int &argc, char **argv);

    /**
     * Constructor. Pass command-line arguments.
     *
     * A KConfig object is
     * created that contains an application-specific config file whose
     * name is "~/." + rAppName + "rc". The state of the application-specific
     * config file may be queried afterwards with getConfigState().
     */
    KApplication(int &argc, char **argv, const QString &rAppName);

    /**
     * Destructor
     */
    virtual ~KApplication();

    /**
     * Return the current application object.
     *
     * This is similar to the global QApplication pointer qApp. It allows access
     * to the single global KApplication object, since more than one cannot be
     * created in the same application. It saves the trouble of having to pass
     * the pointer to it explicitly to every function that may require it.
     */
    static KApplication *getKApplication()
    {
        return KApp;
    }

    /**
     * Return the logical application name as set in the constructor.
     */
    const QString &appName() const
    {
        return aAppName;
    }

    /**
     * Retrieve the application config object.
     *
     * @return a pointer to the application's global KConfig object.
     * @see KConfig
     */
    KConfig *getConfig() const
    {
        return pConfig;
    }

    /**
     * Return a standard help menu
     * @param bAboutQtMenu not used anymore
     * @param appAboutText a little text about the application
     * @return a standard help menu
     */
    QPopupMenu *getHelpMenu(bool bAboutQtMenu, const char *appAboutText);

    /**
     * Get a KLocale object for the application. If it does not yet exist,
     * create one.
     * @return a pointer to the KLocale object of the application
     * @see KLocale
     */
    KLocale *getLocale();

    /**
     * Get a KCharsets object for the application.
     * @return a pointer to the KCharsets object of the application
     * @see KCharsets
     */
    KCharsets *getCharsets() const
    {
        return pCharsets;
    }

    /**
     * Get the icon for the application.
     * @return a QPixmap with the icon.
     * @see QPixmap
     */
    QPixmap getIcon() const
    {
        return aIconPixmap;
    }

    /**
     * Sets the widget as the main widget. Reimplementation of the
     * QApplication function of the same name; at the end calls it,
     * but before sets the widget's icon to the application icon.
     */
    void setMainWidget(QWidget *widget);

    /**
     * Possible return values for getConfigState().
     *
     * @see #getConfigState
     */
    enum ConfigState { APPCONFIG_NONE, APPCONFIG_OK };

    /**
     * Retrieve the state of the app-config object.
     *
     * Possible return values are APPCONFIG_NONE (the application
     * config file could not be opened) and APPCONFIG_OK (the
     * application config file is opened).
     *
     * @see #ConfigState
     */
    ConfigState getConfigState() const
    {
        return eConfigState;
    }

    /**
     * Invoke the kdehelp HTML help viewer.
     *
     * @param aFilename	The filename that is to be loaded. Its location
     *			is computed automatically according to the KFSSTND.
     *			If aFilename is empty, the logical appname with .html
     *			appended to it is used.
     * @param aTopic		This allows context-sensitive help. Its value
     *			will be appended to the filename, prefixed with
     *			a "#" (hash) character.
     */
    void invokeHTMLHelp(QString aFilename, QString aTopic) const;

    /**
     * Returns the directory where KDE stores its HTML documentation
     *
     * The default for this directory is $KDEDIR/share/doc/HTML
     * @return the name of the directory
     */
    static const QString &kde_htmldir();

    /**
     * Returns the directory where KDE icons are stored
     *
     * The default for this directory is $KDEDIR/share/icons
     * @return the name of the directory
     */
    static const QString &kde_icondir();

    /**
     * Returns the directory where locale-specific information (like
     * translated on-screen messages are stored
     *
     * The default for this directory is $KDEDIR/share/locale
     * @return the name of the directory
     */
    static const QString &kde_localedir();

    /**
     * Get the local KDE base dir
     *
     * This is usually $HOME/.kde
     *
     * @return the name of the directory
     */
    static QString localkdedir();

    /**
     * Get the local KDE icon dir
     *
     * This is usually $HOME/.kde/share/icons
     */
    static QString localicondir();

    /**
     * Get the local KDE config dir
     *
     * This is usually $HOME/.kde/share/config
     */
    static QString localconfigdir();

    /**
     * Return a text for the window caption.
     *
     * This would be set either by
     * "-caption", otherwise it will be equivalent to the name of the
     * executable.
     */
    const char *getCaption() const;

    /**
     * Returns true if the KLocale object for this application has
     * already been constructed
     *
     * @return whether the KLocale object has already been
     * constructed
     */
    bool localeConstructed() const
    {
        return bLocaleConstructed;
    }

protected:
    /**
     * The current application object.
     */
    static KApplication *KApp;

    /**
     * Get the KDE base dir.
     *
     * This is the value of the KDEDIR
     * environment variable if it is set in the process' environment,
     * the compile time default of, if this was not present, either,
     * /usr/local/kde.
     * @return the KDE base dir
     */
    static QString kdedir();

private slots:
    void appHelpActivated();
    void aboutKDE();
    void aboutApp();
    void aboutQt();

private:
    void *pAppData; // don't touch this without Kalles permission
    KConfig *pConfig; // application config object
    QWidget *pTopWidget;
    QString aAppName; // logical application name
    QString aCaption; // the name for the window title
    ConfigState eConfigState;
    static QStrList *pSearchPaths;
    KLocale *pLocale;
    KCharsets *pCharsets;
    QString aAppAboutString; // The text for the about box
    bool bLocaleConstructed; // has the KLocale object already been constructed
    QPixmap aIconPixmap;

    void init();
    void parseCommandLine(int &, char **); // search for special KDE arguments

    QPixmap loadIcon(const QString &name);

    virtual void setPalette();
    virtual void setStyleAndFont();
    virtual void readSettings();
    void resizeAll();
    virtual void applyGUIStyle(GUIStyle newstyle);

public:
    QColor inactiveTitleColor;
    QColor inactiveTextColor;
    QColor activeTitleColor;
    QColor activeTextColor;
    QColor backgroundColor;
    QColor textColor;
    QColor selectColor;
    QColor selectTextColor;
    QColor windowColor;
    QColor windowTextColor;
    int contrast;
    QFont generalFont;
    GUIStyle applicationStyle;

private:
    KApplication(const KApplication &);
    KApplication &operator=(const KApplication &);
};

#endif
