/* This file is part of the KDE libraries
    Copyright (C) 1997 Matthias Kalle Dalheimer (kalle@kde.org)

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.
*/

#ifndef KCONFIG_H
#define KCONFIG_H

#include <kconfigbase.h>

/**
 * KDE Configuration entries
 *
 * This class implements KDE's default-based configuration system.
 *
 * @author Kalle Dalheimer (kalle@kde.org)
 * @version $Id: kconfig.h,v 1.7 1998/01/18 14:38:48 kulow Exp $
 * @see KApplication::getConfig KConfigBase
 * @short KDE Configuration Management  class
 */
class KConfig : public KConfigBase
{
    Q_OBJECT

    // copy-construction and assignment are not allowed
    KConfig(const KConfig &);
    KConfig &operator=(const KConfig &rConfig);

protected:
    /** Open all appropriate configuration files and pass them on to
     * parseOneConfigFile()
     */
    virtual void parseConfigFiles();

    /** Write back the configuration data.
     */
    bool writeConfigFile(QFile &rFile);

public:
    /**
     * Construct a KConfig object.
     *
     * @param pFile A file to parse in addition to the
     *  normally parsed files.
     */
    KConfig(const char *pFile);

    /**
     * Destructor.
     *
     * Writes back any dirty configuration entries.
     */
    virtual ~KConfig();

    /**
     * Write back the config cache.
     */
    virtual void sync();
};

#endif
