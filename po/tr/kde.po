# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: KDE 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-27 00:27-0500\n"
"PO-Revision-Date: 1998-11-19 18:00+0200\n"
"Last-Translator: Kerem ERZURUMLU & Zerrin �NAL <kerem@linux.org.tri & @>\n"
"Language-Team: Hacettepe Linux Users Group <hacettepe@linux.org.tr>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-9\n"
"Content-Transfer-Encoding:\n"

#: ../src/kdelibs/kaccel.cpp:216
msgid "Open"
msgstr "A�"

#: ../src/kdelibs/kaccel.cpp:220
msgid "New"
msgstr "Yeni"

#: ../src/kdelibs/kaccel.cpp:224
msgid "Close"
msgstr "Kapat"

#: ../src/kdelibs/kaccel.cpp:228
msgid "Save"
msgstr "Kaydet"

#: ../src/kdelibs/kaccel.cpp:232
msgid "Print"
msgstr "Yazd�r"

#: ../src/kdelibs/kaccel.cpp:236
msgid "Quit"
msgstr "��k��"

#: ../src/kdelibs/kaccel.cpp:240
msgid "Cut"
msgstr "Kes"

#: ../src/kdelibs/kaccel.cpp:244
msgid "Copy"
msgstr "Kopya"

#: ../src/kdelibs/kaccel.cpp:248
msgid "Paste"
msgstr "Yap��t�r"

#: ../src/kdelibs/kaccel.cpp:252
msgid "Undo"
msgstr "Geri Al"

#: ../src/kdelibs/kaccel.cpp:256
msgid "Find"
msgstr "Bul"

#: ../src/kdelibs/kaccel.cpp:260
msgid "Replace"
msgstr "De�i�tir"

#: ../src/kdelibs/kaccel.cpp:264
#, fuzzy
msgid "Insert"
msgstr "&Ekle"

#: ../src/kdelibs/kaccel.cpp:268
msgid "Home"
msgstr "Ev"

#: ../src/kdelibs/kaccel.cpp:272
msgid "End"
msgstr ""

#: ../src/kdelibs/kaccel.cpp:276
msgid "Prior"
msgstr ""

#: ../src/kdelibs/kaccel.cpp:280
#, fuzzy
msgid "Next"
msgstr "&Sonraki"

#: ../src/kdelibs/kaccel.cpp:284
msgid "Help"
msgstr "Yard�m"

#: ../src/kdelibs/kaccel.cpp:552 ../src/kdelibs/kaccel.cpp:624
msgid "SHIFT"
msgstr ""

#: ../src/kdelibs/kaccel.cpp:556 ../src/kdelibs/kaccel.cpp:626
msgid "CTRL"
msgstr ""

#: ../src/kdelibs/kaccel.cpp:560 ../src/kdelibs/kaccel.cpp:628
msgid "ALT"
msgstr ""

#: ../src/kdelibs/kapp.cpp:154
msgid "&Contents"
msgstr "&��erik"

#: ../src/kdelibs/kapp.cpp:160
msgid "&About"
msgstr "&Hakk�nda"

#: ../src/kdelibs/kapp.cpp:166
msgid "About &KDE..."
msgstr "&KDE Hakk�nda ..."

#: ../src/kdelibs/kapp.cpp:186
msgid "About KDE"
msgstr "KDE Hakk�nda"

#: ../src/kdelibs/kapp.cpp:187
msgid ""
"\n"
"The KDE Desktop Environment was written by the KDE Team,\n"
"a world-wide network of software engineers committed to\n"
"free software development.\n"
"\n"
"Visit http://www.kde.org for more information on the KDE\n"
"Project. Please consider joining and supporting KDE.\n"
"\n"
"Please report bugs at http://bugs.kde.org.\n"
msgstr ""
"\n"
"KDE Masa�st� Ortam� serbest yaz�l�m geli�tirmesine\n"
"kat�lmas� i�in d�nya �ap�ndaki KDE geli�tirme tak�m�\n"
"taraf�ndan yaz�lm��t�r.\n"
"\n"
"KDE Projesi hakk�nda bilgi i�in http://www.kde.org\n"
"adresine u�ray�n�z. L�tfen KDE'yi desteklemeyi ve\n"
"kat�lmay� d���n�n�z.\n"
"Hatalar� l�tfen http://bugs.kde.org adresine bildiriniz.\n"

#: ../src/kdelibs/kkeydialog.cpp:431
msgid "Configure key bindings"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:444
msgid "&Help"
msgstr "&Yard�m"

#: ../src/kdelibs/kkeydialog.cpp:448
#, fuzzy
msgid "&Defaults"
msgstr "&�ntan�ml�"

#: ../src/kdelibs/kkeydialog.cpp:454
msgid "&OK"
msgstr "&Peki"

#: ../src/kdelibs/kkeydialog.cpp:457
msgid "&Cancel"
msgstr "&Vazge�"

#: ../src/kdelibs/kkeydialog.cpp:513
msgid "Current key"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:525
#, fuzzy
msgid "&Action"
msgstr "&Konum:"

#: ../src/kdelibs/kkeydialog.cpp:557
msgid "Choose a key for the selected action"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:583
msgid "&No key"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:591
#, fuzzy
msgid "&Default key"
msgstr "&�ntan�ml�"

#: ../src/kdelibs/kkeydialog.cpp:599
msgid "&Custom key"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:661
msgid "No keys defined"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:663
msgid "Not configurable"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:771 ../src/kdelibs/kkeydialog.cpp:1085
msgid "Attention : key already used"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:1032
msgid "Press the wanted key"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:1055
msgid "Undefined key"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:1103
msgid "Return to end edition"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:1113
msgid "Incorrect key"
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:1134
#, c-format
msgid ""
"The %s key combination has already been allocated\n"
"to the %s action.\n"
"\n"
"Please choose a unique key combination."
msgstr ""

#: ../src/kdelibs/kkeydialog.cpp:1138
msgid "Key conflict"
msgstr ""
