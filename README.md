# kmines-sc
This is a version of kmines 1.0.1, originally released in 1999, modified to compile in modern systems and with its
dependencies, the similarly aged "kdelibs", stripped down to the bare minimum.

## To do
* Maybe fix that license thing and decide if the option of build a separate kdelibs is actually worthy
* Make the build with CMake actually work - Done
* Search and add the translation (po) files - Done
* Actually make CMake install the executable and shared libraries - Done
* Fix all the TODOs and FIXMEs in the code
