function(qt1_wrap_cpp METASOURCES_VAR)
    foreach(MOC_INPUT ${ARGN})
        cmake_path(ABSOLUTE_PATH MOC_INPUT)
        cmake_path(GET MOC_INPUT STEM LAST_ONLY MOC_NAME)
        string(CONCAT MOC_OUTPUT "moc_" "${MOC_NAME}" ".cpp")
        string(CONCAT MOC_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR} "/" ${MOC_OUTPUT})
        add_custom_command(
            OUTPUT
                ${MOC_OUTPUT}
            COMMAND
                moc -o ${MOC_OUTPUT} ${MOC_INPUT}
        )
        list(APPEND MOC_FILES ${MOC_OUTPUT_PATH})
    endforeach()
    set(${METASOURCES_VAR} ${MOC_FILES} PARENT_SCOPE)
endfunction()
