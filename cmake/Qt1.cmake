include(Qt1Utils)

if(NOT IS_DIRECTORY ${CMAKE_SOURCE_DIR}/qt-1.44)
    message(FATAL_ERROR "Qt 1.44 source dir not found. Be sure to run scripts/patch_qt before calling cmake.")
endif()

set(QT1_DIR ${CMAKE_SOURCE_DIR}/qt-1.44)
set(QT1_SOURCE_DIR ${QT1_DIR}/src)
set(QT1_INCLUDE_DIR ${QT1_DIR}/include)

file(GLOB_RECURSE QT1_SOURCES
    ${QT1_SOURCE_DIR}/dialogs/*.cpp
    ${QT1_SOURCE_DIR}/kernel/*.cpp
    ${QT1_SOURCE_DIR}/tools/*.cpp
    ${QT1_SOURCE_DIR}/widgets/*.cpp
)

add_executable(moc
    ${QT1_SOURCE_DIR}/moc/mocgen.cpp
    ${QT1_SOURCE_DIR}/tools/qbuffer.cpp
    ${QT1_SOURCE_DIR}/tools/qcollection.cpp
    ${QT1_SOURCE_DIR}/tools/qdatetime.cpp
    ${QT1_SOURCE_DIR}/tools/qdatastream.cpp
    ${QT1_SOURCE_DIR}/tools/qgarray.cpp
    ${QT1_SOURCE_DIR}/tools/qgdict.cpp
    ${QT1_SOURCE_DIR}/tools/qglist.cpp
    ${QT1_SOURCE_DIR}/tools/qglobal.cpp
    ${QT1_SOURCE_DIR}/tools/qgvector.cpp
    ${QT1_SOURCE_DIR}/tools/qiodevice.cpp
    ${QT1_SOURCE_DIR}/tools/qstring.cpp
)
target_include_directories(moc PRIVATE ${QT1_INCLUDE_DIR})

qt1_wrap_cpp(QT1_METASOURCES
    ${QT1_SOURCE_DIR}/dialogs/qfiledialog.h
    ${QT1_SOURCE_DIR}/dialogs/qmessagebox.h
    ${QT1_SOURCE_DIR}/dialogs/qprintdialog.h
    ${QT1_SOURCE_DIR}/dialogs/qprogressdialog.h
    ${QT1_SOURCE_DIR}/dialogs/qtabdialog.h
    ${QT1_SOURCE_DIR}/kernel/qaccel.h
    ${QT1_SOURCE_DIR}/kernel/qapplication.h
    ${QT1_SOURCE_DIR}/kernel/qasyncio.h
    ${QT1_SOURCE_DIR}/kernel/qclipboard.h
    ${QT1_SOURCE_DIR}/kernel/qdialog.h
    ${QT1_SOURCE_DIR}/kernel/qdragobject.h
    ${QT1_SOURCE_DIR}/kernel/qgmanager.h
    ${QT1_SOURCE_DIR}/kernel/qlayout.h
    ${QT1_SOURCE_DIR}/kernel/qsemimodal.h
    ${QT1_SOURCE_DIR}/kernel/qsignalmapper.h
    ${QT1_SOURCE_DIR}/kernel/qsocketnotifier.h
    ${QT1_SOURCE_DIR}/kernel/qtimer.h
    ${QT1_SOURCE_DIR}/kernel/qwidget.h
    ${QT1_SOURCE_DIR}/kernel/qwindow.h
    ${QT1_SOURCE_DIR}/widgets/qbuttongroup.h
    ${QT1_SOURCE_DIR}/widgets/qbutton.h
    ${QT1_SOURCE_DIR}/widgets/qcheckbox.h
    ${QT1_SOURCE_DIR}/widgets/qcombobox.h
    ${QT1_SOURCE_DIR}/widgets/qframe.h
    ${QT1_SOURCE_DIR}/widgets/qgroupbox.h
    ${QT1_SOURCE_DIR}/widgets/qheader.h
    ${QT1_SOURCE_DIR}/widgets/qlabel.h
    ${QT1_SOURCE_DIR}/widgets/qlcdnumber.h
    ${QT1_SOURCE_DIR}/widgets/qlineedit.h
    ${QT1_SOURCE_DIR}/widgets/qlistbox.h
    ${QT1_SOURCE_DIR}/widgets/qlistview.h
    ${QT1_SOURCE_DIR}/widgets/qmainwindow.h
    ${QT1_SOURCE_DIR}/widgets/qmenubar.h
    ${QT1_SOURCE_DIR}/widgets/qmultilinedit.h
    ${QT1_SOURCE_DIR}/widgets/qpopupmenu.h
    ${QT1_SOURCE_DIR}/widgets/qprogressbar.h
    ${QT1_SOURCE_DIR}/widgets/qpushbutton.h
    ${QT1_SOURCE_DIR}/widgets/qradiobutton.h
    ${QT1_SOURCE_DIR}/widgets/qscrollbar.h
    ${QT1_SOURCE_DIR}/widgets/qscrollview.h
    ${QT1_SOURCE_DIR}/widgets/qslider.h
    ${QT1_SOURCE_DIR}/widgets/qspinbox.h
    ${QT1_SOURCE_DIR}/widgets/qsplitter.h
    ${QT1_SOURCE_DIR}/widgets/qstatusbar.h
    ${QT1_SOURCE_DIR}/widgets/qtabbar.h
    ${QT1_SOURCE_DIR}/widgets/qtableview.h
    ${QT1_SOURCE_DIR}/widgets/qtoolbar.h
    ${QT1_SOURCE_DIR}/widgets/qtoolbutton.h
    ${QT1_SOURCE_DIR}/widgets/qtooltip.h
    ${QT1_SOURCE_DIR}/widgets/qvalidator.h
    ${QT1_SOURCE_DIR}/widgets/qwellarray.h
    ${QT1_SOURCE_DIR}/widgets/qwidgetstack.h
)
add_library(qt1 SHARED
    ${QT1_SOURCES}
    ${QT1_METASOURCES}
)
target_include_directories(qt1 PUBLIC ${QT1_INCLUDE_DIR})
target_link_libraries(qt1 PRIVATE X11::X11)
set_property(TARGET qt1 PROPERTY SOVERSION 1.44)
add_library(Qt1 ALIAS qt1)
install(TARGETS qt1)
